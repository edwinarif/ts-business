export default (componentIds: string[]) => `
import React from 'react'
{{{imports}}}

function {{componentName}}(props) {
  return (
    ${componentIds.length > 1 ? '<>' : ''}
    {{#if childrens}}
    {{> partialChildrens}}
    {{else}}
    null
    {{/if}}
    ${componentIds.length > 1 ? '</>' : ''}
  )
}

export default {{componentName}}

`;

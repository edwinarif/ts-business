import { omit } from 'utils/mixins';
import { isString } from 'util';
import parseProps from 'utils/parseProps';
import componentsMapper, { BlockTypes } from 'componentsMapper';
import generatePropsFromDefault from 'utils/generatePropsFromDefault';
import { ComponentFirebaseType } from 'types';

const getChildrens = (component: ComponentFirebaseType): any => {
  const componentConfig = componentsMapper[component.blockType as BlockTypes];
  const { componentName } = componentConfig;
  const componentProps = generatePropsFromDefault(
    component.blockType,
    component.props
  );

  const base = {
    componentType: component.blockType,
    componentName,
    content:
      componentProps &&
      componentProps.children &&
      isString(componentProps.children)
        ? componentProps.children
        : null,
    childrens:
      component.componentIds && component.componentIds.length > 0
        ? component.componentIds.map(id =>
            getChildrens(component.components![id])
          )
        : [],
    props: componentProps
      ? parseProps(componentProps, componentConfig)
      : undefined,
  };
  return base.childrens.length === 0 ? omit(base, ['childrens']) : base;
};

export default getChildrens;

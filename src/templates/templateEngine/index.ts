import Handlebars from 'handlebars';
import prettier from 'prettier/standalone';
import parserBabel from 'prettier/parser-babylon';
import getChildrens from './getChildrens';
import baseTemplate from './baseTemplate';
import partialChildrensTemplate from './partialChildrensTemplate';
import getImports from './getImports';
import { ComponentFirebaseType } from 'types';

export default (
  componentIds: string[],
  components: { [id: string]: ComponentFirebaseType }
) => {
  const childrens = componentIds.map(id => getChildrens(components[id]));
  const imports = getImports(childrens);
  const template = Handlebars.compile(baseTemplate(componentIds));
  const partialChildrens = Handlebars.compile(partialChildrensTemplate);
  Handlebars.registerPartial('partialChildrens', partialChildrens);
  const context = {
    componentName: 'MyComponent',
    childrens,
    imports,
  };
  const baseCode = template(context);
  const code = prettier.format(baseCode, {
    parser: 'babel',
    plugins: [parserBabel],
  });

  return code;
};

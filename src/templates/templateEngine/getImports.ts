import componentsMapper, { BlockTypes } from 'componentsMapper';

const getAllComponentTypeUnique = (
  childrens: Array<any>
): Array<BlockTypes> => {
  const res = childrens.reduce(
    (prev, curr) => {
      const childrensImport = curr.childrens
        ? getAllComponentTypeUnique(curr.childrens)
        : [];

      const componentType = curr.componentType as BlockTypes;
      return Array.from(new Set([...prev, ...childrensImport, componentType]));
    },
    [] as Array<BlockTypes>
  );

  return res;
};

const getImports = (childrens: Array<any>) => {
  const componentTypes = getAllComponentTypeUnique(childrens);
  return componentTypes.reduce((prev, curr) => {
    const importStatement = componentsMapper[curr].importStatement;
    return `${prev}
      ${importStatement}`;
  }, '');
};

export default getImports;

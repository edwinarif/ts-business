import React from 'react';
import './styles.css';

const TestComponent = () => {
  return (
    <>
      <div className='testcomponent'>
        <span>TestComponent</span>
      </div>
    </>
  );
};

export default TestComponent;

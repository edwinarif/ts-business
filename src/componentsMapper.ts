import { CSSProperties } from '@material-ui/styles';
import muiConstants from './muiConstants';
import Grid, { GridProps } from 'testcore/Grid';
import Button, { ButtonProps } from 'testcore/Button';
import Typography, { TypographyProps } from 'testcore/Typography';
import Paper, { PaperProps } from 'testcore/Paper';
import Checkbox, { CheckboxProps } from 'testcore/Checkbox';
import Slider, { SliderProps } from 'testcore/Slider';
import Container, { ContainerProps } from 'testcore/Container';

export type BlockTypes =
  | 'MuiContainer'
  | 'MuiGrid'
  | 'MuiButton'
  | 'MuiTypography'
  | 'MuiPaper'
  | 'MuiCheckbox'
  | 'MuiSlider';
export type ComponentProps =
  | ContainerProps
  | GridProps
  | ButtonProps
  | TypographyProps
  | PaperProps
  | CheckboxProps
  | SliderProps;

export type ComponentMapBase = {
  Component: React.FunctionComponent | React.ComponentType;
  componentName: string;
  importStatement: string;
  style?: CSSProperties;
};

export type PropsConfig<T extends ComponentProps> = {
  [key in keyof T]: {
    default: T[key];
    value?: T[key];
    inputType: 'TextField' | 'Switch' | 'Select';
    label?: string;
    options?: Array<any>;
    required?: boolean;
  };
};

export type ComponentMapperTyped<T extends ComponentProps> = {
  propsConfig: PropsConfig<T>;
} & ComponentMapBase;

export type ComponentMapper = {
  [key in BlockTypes]: ComponentMapperTyped<ComponentProps>;
};

const getMuiCoreImport = (componentName: string) =>
  `import {${componentName}} from '@material-ui/core'`;

const componentsMapper: ComponentMapper = {
  MuiContainer: {
    Component: Container,
    importStatement: getMuiCoreImport('Container'),
    componentName: 'Container',
    propsConfig: {
      children: { default: '' },
      fixed: {
        default: false,
        inputType: 'Switch',
      },
      maxWidth: {
        default: 'lg',
        inputType: 'Select',
        options: ['xs', 'sm', 'md', 'lg', 'xl', false],
      },
    },
  } as ComponentMapperTyped<ContainerProps>,
  MuiButton: {
    Component: Button,
    importStatement: getMuiCoreImport('Button'),
    componentName: 'Button',
    propsConfig: {
      children: {
        default: '',
        value: 'Button',
        label: 'Text',
        inputType: 'TextField',
        required: true,
      },
      // href: {
      //   default: '',
      //   label: 'href',
      //   componentType: 'TextField',
      // },
      color: {
        default: 'default',
        value: 'primary',
        inputType: 'Select',
        options: muiConstants.color,
      },
      variant: {
        default: 'text',
        value: 'contained',
        inputType: 'Select',
        options: ['text', 'outlined', 'contained'],
      },
      size: {
        default: 'medium',
        inputType: 'Select',
        options: ['small', 'medium', 'large'],
      },
      fullWidth: { default: false, inputType: 'Switch' },
      disabled: { default: false, inputType: 'Switch' },
      disableRipple: { default: false, inputType: 'Switch' },
      disableFocusRipple: { default: false, inputType: 'Switch' },
    },
    customConfig: {
      droppable: false,
    },
  } as ComponentMapperTyped<ButtonProps>,
  MuiPaper: {
    Component: Paper,
    componentName: 'Paper',
    importStatement: getMuiCoreImport('Paper'),
    propsConfig: {
      elevation: {
        default: 1,
        label: 'elevation',
        inputType: 'TextField',
      },
      square: {
        default: false,
        inputType: 'Switch',
      },
    },
  } as ComponentMapperTyped<PaperProps>,
  MuiTypography: {
    Component: Typography,
    componentName: 'Typography',
    importStatement: getMuiCoreImport('Typography'),
    propsConfig: {
      variant: {
        default: 'body1',
        value: 'h5',
        inputType: 'Select',
        options: [...muiConstants.themeStyle, 'srOnly', 'inherit'],
      },
      align: {
        default: 'inherit',
        inputType: 'Select',
        options: muiConstants.align,
      },
      children: {
        default: '',
        value: 'Typography',
        label: 'Label',
        inputType: 'TextField',
      },
      color: {
        default: 'initial',
        value: 'primary',
        inputType: 'Select',
        options: [
          'initial',
          'inherit',
          'primary',
          'secondary',
          'textPrimary',
          'textSecondary',
          'error',
        ],
      },
      display: {
        default: 'initial',
        inputType: 'Select',
        options: ['initial', 'block', 'inline'],
      },
      gutterBottom: { default: false, inputType: 'Switch' },
      noWrap: { default: false, inputType: 'Switch' },
      paragraph: { default: false, inputType: 'Switch' },
    },
    customConfig: {
      droppable: false,
    },
  } as ComponentMapperTyped<TypographyProps>,
  MuiSlider: {
    Component: Slider,
    componentName: 'Slider',
    importStatement: getMuiCoreImport('Slider'),
    propsConfig: {
      disabled: {
        default: false,
        inputType: 'Switch',
      },
      min: {
        default: 0,
        inputType: 'TextField',
      },
      max: {
        default: 100,
        inputType: 'TextField',
      },
      step: {
        default: 1,
        inputType: 'TextField',
      },
      orientation: {
        default: 'horizontal',
        inputType: 'Select',
        options: ['horizontal', 'vertical'],
      },
      valueLabelDisplay: {
        default: 'off',
        inputType: 'Select',
        options: ['on', 'auto', 'off'],
      },
    },
    customConfig: {
      droppable: false,
    },
  } as ComponentMapperTyped<SliderProps>,
  MuiCheckbox: {
    Component: Checkbox,
    componentName: 'Checkbox',
    importStatement: getMuiCoreImport('Checkbox'),
    propsConfig: {
      id: {
        default: '',
        inputType: 'TextField',
      },
      checked: {
        default: true,
        inputType: 'Switch',
      },
      color: {
        default: 'secondary',
        inputType: 'Select',
        options: ['primary', 'secondary', 'default'],
      },
      disabled: {
        default: false,
        inputType: 'Switch',
      },
      disableRipple: {
        default: false,
        inputType: 'Switch',
      },
      indeterminate: {
        default: false,
        inputType: 'Switch',
      },
    },
    customConfig: {
      droppable: false,
    },
  } as ComponentMapperTyped<CheckboxProps>,
  MuiGrid: {
    Component: Grid,
    componentName: 'Grid',
    importStatement: getMuiCoreImport('Grid'),
    propsConfig: {
      children: { default: '', label: 'Label', inputType: 'TextField' },
      container: { default: false, inputType: 'Switch' },
      item: { default: false, inputType: 'Switch' },
      alignContent: {
        default: 'stretch',
        inputType: 'Select',
        options: [
          'stretch',
          'center',
          'flex-start',
          'flex-end',
          'space-between',
          'space-around',
        ],
      },
      alignItems: {
        default: 'stretch',
        inputType: 'Select',
        options: ['flex-start', 'center', 'flex-end', 'stretch', 'baseline'],
      },
      direction: {
        default: 'row',
        inputType: 'Select',
        options: ['row', 'row-reverse', 'column', 'column-reverse'],
      },
      spacing: {
        default: 0,
        inputType: 'Select',
        options: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      },
      justify: {
        default: 'flex-start',
        inputType: 'Select',
        options: [
          'flex-start',
          'center',
          'flex-end',
          'space-between',
          'space-around',
          'space-evenly',
        ],
      },
      wrap: {
        default: 'wrap',
        inputType: 'Select',
        options: ['nowrap', 'wrap', 'wrap-reverse'],
      },
      xs: {
        default: false,
        inputType: 'Select',
        options: muiConstants.gridSize,
      },
      sm: {
        default: false,
        inputType: 'Select',
        options: muiConstants.gridSize,
      },
      md: {
        default: false,
        inputType: 'Select',
        options: muiConstants.gridSize,
      },
      lg: {
        default: false,
        inputType: 'Select',
        options: muiConstants.gridSize,
      },
      xl: {
        default: false,
        inputType: 'Select',
        options: muiConstants.gridSize,
      },
    },
  } as ComponentMapperTyped<GridProps>,
};

export const componentsKeys = Object.keys(componentsMapper) as Array<
  BlockTypes
>;

export const blockCategories = {
  Layouts: null,
  Buttons: null,
  Typographies: null,
};
export type BlockCategories = keyof typeof blockCategories;
export const categoryKeys = Object.keys(blockCategories) as BlockCategories[];

export type DefaultBlock = {
  id: string;
  blockType: BlockTypes;
  label?: string;
  props?: ComponentProps;
  strCss?: string;
  category?: BlockCategories;
  acceptDrop?: string;
  blockStyleCss?: CSSProperties;
};

export type DefaultBlocks = {
  [key in BlockCategories]: DefaultBlock[];
};

export const defaultBlocks: DefaultBlocks = {
  Layouts: [
    {
      id: 'AgMJBwcDAg4',
      blockType: 'MuiContainer',
      label: 'Container',
      acceptDrop: 'box',
      blockStyleCss: {
        outline: '2px dashed rgba(41,182,246,1)',
      },
    },
    {
      id: 'AAYECwcCAwI',
      blockType: 'MuiPaper',
      label: 'Paper',
      acceptDrop: 'box',
    },
  ],
  Buttons: [
    {
      id: 'BAcMDAcGCw8',
      blockType: 'MuiButton',
      label: 'Button Secondary',
      props: {
        color: 'secondary',
      } as ButtonProps,
    },
    {
      id: 'BA4ADwkICQs',
      blockType: 'MuiButton',
      label: 'Button Outlined',
      props: {
        variant: 'outlined',
      } as ButtonProps,
    },
    {
      id: 'BAwLBgAJDgo',
      blockType: 'MuiButton',
      label: 'Button Outlined Secondary',
      props: {
        color: 'secondary',
        variant: 'outlined',
      } as ButtonProps,
    },
    {
      id: 'CA8HCAwKCAs',
      blockType: 'MuiButton',
      label: 'Button Text',
      props: {
        variant: 'text',
      } as ButtonProps,
    },
    {
      id: 'Bw8GBAUACg4',
      blockType: 'MuiButton',
      label: 'Button Text Secondary',
      props: {
        color: 'secondary',
        variant: 'text',
      } as ButtonProps,
    },
  ],
  Typographies: [
    {
      id: 'BwALAAgPCQk',
      blockType: 'MuiTypography',
      label: 'Typography / Text',
      props: { display: 'inline' } as TypographyProps,
    },
  ],
};

export const getBlockList = () => {
  return (Object.keys(defaultBlocks) as BlockCategories[]).reduce(
    (prev, category) => {
      const blocks = defaultBlocks[category].reduce(
        (prev, block) => {
          return {
            ...prev,
            [block.id]: {
              ...block,
              category,
            },
          };
        },
        {} as { [key: string]: DefaultBlock }
      );

      return { ...prev, ...blocks };
    },
    {} as { [key: string]: DefaultBlock }
  );
};

export default componentsMapper;

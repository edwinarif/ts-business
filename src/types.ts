import { ComponentProps, BlockCategories, BlockTypes } from 'componentsMapper';
import { CSSProperties } from '@material-ui/styles';

export interface IThemeFirebase {
  name: string;
  config: object;
  ownerId: string;
  createdAt?: string | number | Date;
  updatedAt?: string | number | Date;
}

export type ComponentFirebaseType = {
  id: string;
  blockType: BlockTypes;
  blockId: string;
  label?: string;
  category?: BlockCategories;
  props?: ComponentProps;
  strCss?: string;
  acceptDrop?: string;
  blockStyleCss?: CSSProperties;
} & NestedComponentType;

export type NestedComponentType = {
  components?: { [key: string]: ComponentFirebaseType };
  componentIds?: string[];
};

export type ComponentType = {
  baseHtml: string;
  baseCss: string;
  isSelected?: boolean;
  isDragging?: boolean;
} & ComponentFirebaseType;

export interface IPage {
  name: string;
  themeId: string;
  blockIds?: {
    [key: string]: {
      baseBlockId: string;
    };
  };
  rootComponent?: NestedComponentType;
  isAsComponent?: boolean;
  createdAt?: string | number | Date;
  updatedAt?: string | number | Date;
}

export interface IProject {
  name: string;
  defaultPageId?: string;
  pages?: {
    [key: string]: IPage;
  };
  ownerId: string;
  defaultThemeId?: string;
  createdAt?: string | number | Date;
  updatedAt?: string | number | Date;
}

export type FirebaseUser = firebase.User & {
  isEmpty: boolean;
  isLoaded: boolean;
};

export interface FirebaseState {
  isInitializing?: boolean;
  auth: FirebaseUser;
  data: {
    users?: any;
    projects?: {
      [key: string]: IProject;
    };
    blocks?: {
      [key: string]: ComponentFirebaseType;
    };
    themes?: {
      [key: string]: IThemeFirebase;
    };
  };
}

export interface DragItem {
  index: number;
  id: string;
  type: string;
}

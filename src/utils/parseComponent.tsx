import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { ServerStyleSheets, ThemeProvider } from '@material-ui/styles';
import { config } from 'themes/template1';
import componentsMapper, { ComponentProps } from 'componentsMapper';
import { createMuiTheme } from '@material-ui/core';
import { ExtendedFirebaseInstance } from 'react-redux-firebase';
import { Dispatch } from 'redux';
import { ComponentFirebaseType, ComponentType } from 'types';
import { addBlock } from 'redux/actions/localActions';
import generatePropsFromDefault from './generatePropsFromDefault';
import classesAddId from './classesAddId';
import getClasses from './getClasses';

const sheets = new ServerStyleSheets();

export const getHtmlStyle = (
  dispatch: Dispatch<any>,
  themeConfig: any,
  componentMapper: any,
  blockFirebase: ComponentFirebaseType,
  componentId: string,
  componentProps?: ComponentProps
) => {
  const outerTheme = createMuiTheme();
  const theme = createMuiTheme(themeConfig);
  const { Component } = componentMapper;
  const customProps = generatePropsFromDefault(blockFirebase.blockType, {
    ...blockFirebase.props,
    ...(componentProps || {}),
  } as any);
  const component = (
    <ThemeProvider theme={outerTheme}>
      <ThemeProvider theme={() => theme}>
        <Component {...customProps} />
      </ThemeProvider>
    </ThemeProvider>
  );
  const html = ReactDOMServer.renderToStaticMarkup(sheets.collect(component));
  const style = sheets.toString();
  const classes = getClasses(html);
  const block: ComponentType = {
    ...blockFirebase,
    baseHtml: classesAddId(classes, html, componentId || blockFirebase.id),
    baseCss: classesAddId(classes, style, componentId || blockFirebase.id),
    isDragging: false,
    isSelected: false,
  };

  if (componentId !== '') {
    const res = { ...block, id: componentId, props: customProps };
    // console.log('sini', res);
    // dispatch(addComponent(res));
    return res;
  } else {
    dispatch(addBlock(block));
    return block;
  }
};

export const parseComponent = (
  dispatch: Dispatch<any>,
  firebase: ExtendedFirebaseInstance,
  blockFirebase: ComponentFirebaseType,
  themeId?: string,
  componentId: string = '',
  componentProps?: ComponentProps,
  themeConfig?: string
) => {
  if (!blockFirebase) return;

  const componentMapper = componentsMapper[blockFirebase.blockType];
  if (!componentMapper) return undefined;

  if (themeId) {
    const ref = firebase.ref(`/themes/${themeId}/config`);
    ref.once('value', function(snapshot) {
      let customThemeConfig;
      try {
        customThemeConfig = JSON.parse(snapshot.val());
      } catch {
        customThemeConfig = config;
      }

      return getHtmlStyle(
        dispatch,
        customThemeConfig,
        componentMapper,
        blockFirebase,
        componentId,
        componentProps
      );
    });
  } else if (themeConfig) {
    return getHtmlStyle(
      dispatch,
      JSON.parse(themeConfig),
      componentMapper,
      blockFirebase,
      componentId,
      componentProps
    );
  }

  return undefined;
};

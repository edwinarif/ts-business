import prettier from 'prettier/standalone';
import parserCss from 'prettier/parser-postcss';

export default (css: string): Boolean => {
  var cssRes = undefined;
  try {
    cssRes = prettier.format(`.root{${css}}` as string, {
      parser: 'css',
      plugins: [parserCss],
    });
  } catch {
    cssRes = undefined;
  }

  if (cssRes) return true;

  return false;
};

import { ComponentProps, ComponentMapperTyped } from 'componentsMapper';
import { omit } from './mixins';

export default (
  baseProps: ComponentProps,
  componentConfig: ComponentMapperTyped<ComponentProps>
): string => {
  const props = omit(baseProps, ['children']);

  const res = (Object.keys(props) as Array<keyof ComponentProps>).reduce(
    (prev, curr) => {
      if (typeof props[curr] === 'string')
        return `${prev} ${curr}="${props[curr]}"`;
      if (
        typeof props[curr] === 'boolean' &&
        !props[curr] &&
        props[curr] === componentConfig.propsConfig[curr]!.default
      )
        return prev;
      if (typeof props[curr] === 'boolean' && props[curr])
        return `${prev} ${curr}`;
      if (typeof props[curr] === 'boolean' && !props[curr])
        return `${prev} ${curr}={false}`;
      return `${prev} ${curr}=${props[curr]}`;
    },
    ''
  );

  return res;
};

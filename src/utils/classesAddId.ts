export default (classes: Array<string>, str: string, id: string) =>
  classes.reduce((res: string, val) => {
    const strHtml = res === '' ? str : res;
    return strHtml.replace(new RegExp(val, 'g'), `${val}-${id}`);
  }, '');

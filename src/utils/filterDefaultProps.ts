import componentsMapper, { ComponentProps, BlockTypes } from 'componentsMapper';

export default (componentName: BlockTypes, formValues: ComponentProps) =>
  (Object.keys(formValues) as Array<keyof ComponentProps>).reduce(
    (prev, curr) => {
      if (
        formValues[curr] ===
        componentsMapper[componentName].propsConfig[curr]!.default
      )
        return prev;

      return {
        ...prev,
        [curr]: formValues[curr],
      };
    },
    {} as any
  );

import componentsMapper, { BlockTypes, ComponentProps } from 'componentsMapper';

export default (
  componentType: BlockTypes,
  customProps?: ComponentProps
): ComponentProps => {
  const res = {
    ...(Object.keys(componentsMapper[componentType].propsConfig) as Array<
      keyof ComponentProps
    >).reduce((prev, current) => {
      const propsConfig = componentsMapper[componentType].propsConfig;
      if (customProps && customProps[current]) {
        return { ...prev, [current]: customProps[current] };
      }

      if (propsConfig[current] && propsConfig[current]!.value)
        return {
          ...prev,
          [current]: propsConfig[current]!.value,
        };

      return prev;
    }, {}),
  };

  return res;
};

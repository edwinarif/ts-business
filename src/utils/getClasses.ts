export default (html: string) => {
  const regexLiteral = /class="(.*?)"/g;
  const matches = html.match(regexLiteral);

  if (matches === null) return [];

  return matches.reduce((res: Array<string>, val) => {
    const className = val
      .replace('class="', '')
      .replace('"', '')
      .split(' ');
    return Array.from(new Set([...res, ...className]));
  }, []);
};

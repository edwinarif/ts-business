export const firebaseConfig = {
  apiKey: 'AIzaSyAZoOJQg7bj0pIcGRwkb_HNX-MlVERqMCE',
  authDomain: 'ts-business.firebaseapp.com',
  databaseURL: 'https://ts-business.firebaseio.com',
  projectId: 'ts-business',
  storageBucket: '',
  messagingSenderId: '933804016168',
  appId: '1:933804016168:web:607d58c198c6c60b',
};

export const reduxFirebase = {
  enableLogging: false,
};

export default { firebaseConfig, reduxFirebase };

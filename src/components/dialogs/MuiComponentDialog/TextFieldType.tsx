import React from 'react';
import { TextField } from '@material-ui/core';

type TextFieldTypeProps = {
  fieldId: string;
  value: string;
  label?: string;
  onChange: (id: string) => (event: any) => void;
};

const TextFieldType = ({
  label,
  onChange,
  fieldId,
  value,
}: TextFieldTypeProps) => {
  return (
    <TextField
      id={`textField-${fieldId}`}
      label={label || fieldId}
      value={value}
      onChange={onChange(fieldId)}
    />
  );
};

export default TextFieldType;

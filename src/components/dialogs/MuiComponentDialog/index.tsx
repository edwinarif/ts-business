import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import {
  makeStyles,
  useTheme,
  createStyles,
  Dialog,
  DialogTitle,
  DialogContent,
  useMediaQuery,
  Paper,
  DialogActions,
  Button,
} from '@material-ui/core';
import { ComponentProps, ComponentMapperTyped } from 'componentsMapper';
import TextFieldType from './TextFieldType';
import SwitchType from './SwitchType';
import SelectType from './SelectType';
import MuiComponent from 'components/MuiComponent';

type MuiComponentDialogProps<T extends ComponentProps> = {
  title: string;
  isOpen: boolean;
  handleClose: (values?: T) => () => void;
  mapper: ComponentMapperTyped<T>;
};

function MuiComponentDialog<T extends ComponentProps>({
  title,
  isOpen,
  handleClose,
  mapper,
}: MuiComponentDialogProps<T>) {
  const theme = useTheme();
  const classes = useStyles();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const { propsConfig, ...componentProps } = mapper;
  const configKeys = Object.keys(propsConfig) as Array<
    keyof typeof propsConfig
  >;
  const defaultState = configKeys.reduce(
    (res, key) => ({
      ...res,
      [key]: propsConfig[key].default,
    }),
    {} as T
  );
  const [propsValues, setPropsValues] = useState<T>(defaultState);

  const handleChange = (id: string) => (event: any) => {
    setPropsValues({
      ...propsValues,
      [id]: event.target.value,
    });
  };

  const handleChecked = (id: string) => (
    event: React.ChangeEvent,
    value: boolean
  ) => {
    setPropsValues({
      ...propsValues,
      [id]: value,
    });
  };

  return (
    <Dialog fullScreen={fullScreen} open={isOpen} onClose={handleClose()}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <Grid container spacing={1}>
          <Grid item xs={6}>
            <Grid container spacing={4}>
              {configKeys.map(key => {
                if (propsConfig[key].inputType === 'TextField') {
                  return (
                    <TextFieldType
                      key={`item-${key}`}
                      fieldId={key as string}
                      value={(propsValues[key] as unknown) as string}
                      label={propsConfig[key]!.label}
                      onChange={handleChange}
                    />
                  );
                }

                if (propsConfig[key]!.inputType === 'Switch') {
                  return (
                    <SwitchType
                      key={`item-${key}`}
                      fieldId={key as string}
                      checked={(propsValues[key] as unknown) as boolean}
                      label={propsConfig[key]!.label}
                      onChange={handleChecked}
                    />
                  );
                }

                if (propsConfig[key]!.inputType === 'Select') {
                  return (
                    <SelectType
                      key={`item-${key}`}
                      fieldId={key as string}
                      value={propsValues[key]}
                      label={propsConfig[key]!.label}
                      onChange={handleChange}
                      options={propsConfig[key]!.options || []}
                    />
                  );
                }

                return (
                  <Grid item key={`item-${key}`}>
                    {key}
                  </Grid>
                );
              })}
            </Grid>
          </Grid>
          <Grid item xs={6}>
            <Paper elevation={1} className={classes.paperRoot}>
              <MuiComponent {...componentProps} props={propsValues} />
            </Paper>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose()} color='primary'>
          Cancel
        </Button>
        <Button onClick={handleClose(propsValues)} color='primary'>
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const useStyles = makeStyles(() =>
  createStyles({
    paperRoot: {
      height: '100%',
      textAlign: 'center',
      lineHeight: 24,
    },
  })
);

export default MuiComponentDialog;

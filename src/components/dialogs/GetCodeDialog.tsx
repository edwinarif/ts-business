import React from 'react';
import {
  Dialog,
  DialogContent,
  CircularProgress,
  useMediaQuery,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { tomorrow } from 'react-syntax-highlighter/dist/esm/styles/hljs';

type GetCodeDialogProps = {
  isOpen: boolean;
  handleClose: () => void;
  codeValue?: string;
  isLoading?: boolean;
};

const GetCodeDialog = ({
  isOpen,
  handleClose,
  codeValue,
  isLoading,
}: GetCodeDialogProps) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Dialog fullScreen={fullScreen} open={isOpen} onClose={handleClose}>
      <DialogContent>
        {isLoading ? (
          <div>
            <CircularProgress />
          </div>
        ) : (
          <SyntaxHighlighter language='jsx' style={tomorrow}>
            {codeValue || 'No generated code'}
          </SyntaxHighlighter>
        )}
      </DialogContent>
    </Dialog>
  );
};

export default GetCodeDialog;

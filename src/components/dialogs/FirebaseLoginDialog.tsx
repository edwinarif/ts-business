import React from 'react';
import * as firebase from 'firebase/app';
import { firebaseAuth } from 'index';
import { StyledFirebaseAuth } from 'react-firebaseui';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

type FirebaseLoginDialogProps = {
  isOpen: boolean;
  handleClose: () => void;
};

const FirebaseLoginDialog = ({
  isOpen,
  handleClose,
}: FirebaseLoginDialogProps) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const uiConfig = {
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID,
    ],
    signInFlow: 'popup',
  };

  return (
    <Dialog fullScreen={fullScreen} open={isOpen} onClose={handleClose}>
      <DialogContent>
        <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebaseAuth} />
      </DialogContent>
    </Dialog>
  );
};

export default FirebaseLoginDialog;

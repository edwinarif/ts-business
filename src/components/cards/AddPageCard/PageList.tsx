import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import File from '@material-ui/icons/InsertDriveFile';
import MoreVert from '@material-ui/icons/MoreVert';
import { ListItemSecondaryAction, IconButton } from '@material-ui/core';
import { IProject } from 'types';
import { useDispatch } from 'react-redux';
import { setCurrentPage, setIsAddPageOpen } from 'redux/actions/localActions';

const PageList = ({ project }: { project?: IProject }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const pageKeys = Object.keys(project && project.pages ? project.pages : {});

  const handleClick = (pageId: string) => () => {
    if (project) {
      const page = project.pages![pageId];
      dispatch(setCurrentPage(pageId, page));
      dispatch(setIsAddPageOpen(false));
    }
  };

  return (
    <List className={classes.root} dense>
      {pageKeys.map(key => (
        <ListItem button key={`pageItem-${key}`} onClick={handleClick(key)}>
          <ListItemAvatar>
            <File />
          </ListItemAvatar>
          <ListItemText
            primary={project!.pages![key].name}
            secondary={`Last update: ${new Date(project!.pages![key]
              .updatedAt as number).toUTCString()}`}
          />
          <ListItemSecondaryAction>
            <IconButton edge='end' aria-label='more'>
              <MoreVert />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
      minHeight: 100,
    },
  })
);

export default PageList;

import React, { useState } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {
  Button,
  Dialog,
  DialogContent,
  TextField,
  DialogActions,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { useFirebase } from 'react-redux-firebase';
import { useDispatch } from 'react-redux';
import { setCurrentPage, setIsAddPageOpen } from 'redux/actions/localActions';
import { IPage } from 'types';

const BottomAppBar = ({ currentProjectId, project }: any) => {
  const firebase = useFirebase();
  const dispatch = useDispatch();
  const classes = useStyles();

  const [pageName, setPageName] = useState('');
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const handleCancel = () => {
    setIsDialogOpen(false);
    setPageName('');
  };

  const handleCreate = () => {
    if (project) {
      const name = pageName.trim();
      if (name !== '') {
        const dateNow = Date.now();
        const pageFirebase: any = {
          name,
          themeId: project.defaultThemeId || 'theme1',
          createdAt: dateNow,
          updatedAt: dateNow,
        };
        firebase
          .push(`projects/${currentProjectId}/pages`, pageFirebase)
          .then((res: any) => {
            if (res.key) {
              const page: IPage = { ...pageFirebase, id: res.key };
              dispatch(setCurrentPage(res.key, page));
              dispatch(setIsAddPageOpen(false));
            }
          });
        handleCancel();
      }
    }
  };

  return (
    <>
      <AppBar position='sticky' color='primary' className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Button
            color='inherit'
            className={classes.button}
            onClick={() => setIsDialogOpen(true)}
          >
            <AddIcon className={classes.addIcon} />
            Add Page
          </Button>
        </Toolbar>
      </AppBar>
      <Dialog
        open={isDialogOpen}
        onClose={handleCancel}
        aria-labelledby='form-dialog-title'
      >
        <DialogContent>
          <TextField
            autoFocus
            margin='dense'
            id='name'
            label='Page name'
            value={pageName}
            onChange={e => setPageName(e.target.value)}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCancel} color='secondary'>
            Cancel
          </Button>
          <Button onClick={handleCreate} color='secondary'>
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    appBar: {
      bottom: 0,
    },
    button: {
      paddingLeft: '15px !important',
      paddingRight: '15px !important',
    },
    addIcon: {
      marginRight: 10,
    },
    toolbar: {
      minHeight: 48,
      justifyContent: 'flex-end',
    },
  })
);

export default BottomAppBar;

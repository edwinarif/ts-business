import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { setIsAddPageOpen } from 'redux/actions/localActions';
import ChildCard from 'components/cards/generals/ChildCard';
import PageList from './PageList';
import BottomAppBar from './BottomAppBar';

const AddPageCard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {
    isAddPageOpen,
    isBlockDragging,
    currentProjectId,
    project,
  } = useSelector<AppState, LocalReducerType>(state => state.localReducer);

  return (
    <ChildCard
      isOpen={isAddPageOpen && !isBlockDragging}
      onClickClose={() => dispatch(setIsAddPageOpen(false))}
      cardContentProps={{
        className: classes.cardContent,
      }}
    >
      <div className={classes.wrapper}>
        <PageList project={project} />
        <BottomAppBar currentProjectId={currentProjectId} project={project} />
      </div>
    </ChildCard>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    cardContent: {
      padding: '0 !important',
    },
    wrapper: {},
  })
);

export default AddPageCard;

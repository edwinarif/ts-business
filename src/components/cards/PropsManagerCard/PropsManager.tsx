import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { AppState } from 'index';
import componentsMapper from 'componentsMapper';
import TextFieldType from 'components/dialogs/MuiComponentDialog/TextFieldType';
import SwitchType from 'components/dialogs/MuiComponentDialog/SwitchType';
import SelectType from 'components/dialogs/MuiComponentDialog/SelectType';
import { Grid } from '@material-ui/core';
import { useFirebase } from 'react-redux-firebase';
import at from 'lodash/at';
import generatePropsFromDefault from 'utils/generatePropsFromDefault';
import { ComponentFirebaseType } from 'types';
import { LocalReducerType } from 'redux/reducers/localReducer';
import debounce from 'lodash/debounce';

const PropsManager = () => {
  const firebase = useFirebase();
  const classes = useStyles();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );

  const {
    currentSelectedComponent,
    currentSelectedComponentPath,
    currentProjectId,
    currentPageId,
    page,
  } = localReducer;

  const [propsConfig, setPropsConfig] = useState();
  const [configKeys, setConfigKeys] = useState();
  const [propsState, setPropsState] = useState();
  const [propsManagerText, setPropsManagerText] = useState();

  const [updateValue] = useState<any>(() =>
    debounce(
      (
        propsId,
        value,
        currentProjectId,
        currentPageId,
        currentSelectedComponentPath
      ) => {
        firebase
          .ref(
            `projects/${currentProjectId}/pages/${currentPageId}/${currentSelectedComponentPath.replace(
              /\./g,
              '/'
            )}/props/${propsId}`
          )
          .set(value);
      },
      1000
    )
  );

  useEffect(() => {
    setPropsManagerText(undefined);
    const firebaseComponentBase = at<any>(page, currentSelectedComponentPath);
    const firebaseComponent =
      firebaseComponentBase.length > 0
        ? (firebaseComponentBase[0] as ComponentFirebaseType)
        : undefined;

    if (!firebaseComponent || !firebaseComponent.blockType) return;
    const componentProps = generatePropsFromDefault(
      firebaseComponent.blockType,
      firebaseComponent.props
    );
    const propsConfig =
      componentsMapper[firebaseComponent.blockType].propsConfig;
    const configKeys = Object.keys(propsConfig) as Array<
      keyof typeof propsConfig
    >;
    const propsState = configKeys.reduce(
      (prev, curr) => ({
        ...prev,
        [curr]:
          componentProps[curr] !== undefined
            ? componentProps[curr]
            : componentsMapper[firebaseComponent.blockType].propsConfig[curr]!
                .default,
      }),
      {} as any
    );

    setPropsConfig(propsConfig);
    setConfigKeys(configKeys);
    setPropsState(propsState);

    setPropsManagerText(
      configKeys.reduce(
        (prev, key) => {
          if (propsConfig[key]!.inputType === 'TextField') {
            return {
              ...prev,
              [key]: propsState[key],
            };
          }
          return prev;
        },
        {} as any
      )
    );
  }, [currentSelectedComponent, currentSelectedComponentPath, page]);

  if (!currentSelectedComponent || !configKeys) return null;

  const getPath = (id: string) =>
    `projects/${currentProjectId}/pages/${currentPageId}/${currentSelectedComponentPath.replace(
      /\./g,
      '/'
    )}/props/${id}`;

  const handleChange = (id: string) => (event: any) => {
    firebase.set(getPath(id), event.target.value);
  };

  const handleChecked = (id: string) => (
    event: React.ChangeEvent,
    value: boolean
  ) => {
    firebase.set(getPath(id), value);
  };

  const handleChangeText = (id: string) => (e: any) => {
    setPropsManagerText({
      ...propsManagerText,
      [id]: e.target.value,
    });
    updateValue(
      id,
      e.target.value,
      currentProjectId,
      currentPageId,
      currentSelectedComponentPath
    );
  };

  return (
    <Grid container>
      <Grid item xs={12}>
        {currentSelectedComponent}
      </Grid>
      {configKeys.map((key: any) => (
        <div key={`item-${key}`} className={classes.rootItem}>
          {propsConfig[key]!.inputType === 'TextField' && (
            <Grid item className={classes.gridTextField}>
              {propsManagerText && (
                <TextFieldType
                  fieldId={key as string}
                  value={(propsManagerText[key] as string) || ''}
                  label={propsConfig[key]!.label}
                  onChange={handleChangeText}
                />
              )}
            </Grid>
          )}
          {propsConfig[key]!.inputType === 'Switch' && (
            <Grid item className={classes.gridSwitch}>
              <SwitchType
                key={`item-${key}`}
                fieldId={key as string}
                checked={propsState[key] as boolean}
                label={propsConfig[key]!.label}
                onChange={handleChecked}
              />
            </Grid>
          )}
          {propsConfig[key]!.inputType === 'Select' && (
            <Grid item className={classes.gridSelect}>
              <SelectType
                key={`item-${key}`}
                fieldId={key as string}
                value={propsState[key]}
                label={propsConfig[key]!.label}
                onChange={handleChange}
                options={propsConfig[key]!.options || []}
              />
            </Grid>
          )}
        </div>
      ))}
    </Grid>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    rootItem: {
      marginRight: 15,
    },
    gridTextField: {
      marginBottom: 15,
    },
    gridSwitch: {},
    gridSelect: {
      marginBottom: 15,
    },
  })
);

export default PropsManager;

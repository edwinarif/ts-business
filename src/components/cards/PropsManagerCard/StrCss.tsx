import React, { useState, useEffect } from 'react';
import { Typography, TextField } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/styles';
import { useSelector } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import at from 'lodash/at';
import { ComponentFirebaseType } from 'types';
import { useFirebase } from 'react-redux-firebase';
import debounce from 'lodash/debounce';

const StrCss = () => {
  const classes = useStyles();
  const firebase = useFirebase();
  const {
    currentSelectedComponentPath,
    currentProjectId,
    currentPageId,
    page,
  } = useSelector<AppState, LocalReducerType>(state => state.localReducer);

  const firebaseComponentBase = at<any>(
    page,
    currentSelectedComponentPath
  )[0] as ComponentFirebaseType | undefined;

  const [strCssValue, setStrCssValue] = useState<string>('');

  useEffect(() => {
    setStrCssValue(
      firebaseComponentBase && firebaseComponentBase.strCss
        ? firebaseComponentBase.strCss
        : ''
    );
  }, [firebaseComponentBase]);

  const [updateValue] = useState<any>(() =>
    debounce(
      (
        value,
        currentProjectId,
        currentPageId,
        currentSelectedComponentPath
      ) => {
        const currentPathRef = `projects/${currentProjectId}/pages/${currentPageId}/${currentSelectedComponentPath}`.replace(
          /\./g,
          '/'
        );
        firebase.ref(currentPathRef).update({
          strCss: value || null,
        });
      },
      1000
    )
  );

  const handleChange = (e: any) => {
    setStrCssValue(e.target.value);
    updateValue(
      e.target.value,
      currentProjectId,
      currentPageId,
      currentSelectedComponentPath
    );
  };

  return (
    <>
      <Typography className={classes.header}>Plain CSS</Typography>
      <Typography variant='caption' display='block'>
        .root {'{'}
      </Typography>
      <TextField
        id='standard-multiline-static'
        multiline
        rows='4'
        margin='normal'
        variant='outlined'
        onChange={handleChange}
        value={strCssValue}
      />
      <Typography variant='caption' display='block'>
        }
      </Typography>
    </>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    header: {
      marginBottom: 10,
    },
  })
);

export default StrCss;

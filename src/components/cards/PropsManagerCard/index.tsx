import React from 'react';
import {
  Card,
  CardContent,
  Fade,
  AppBar,
  Tabs,
  Tab,
  Divider,
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Draggable from 'react-draggable';
import PropsManager from './PropsManager';
import {
  setPropsManagerTabIdx,
  setIsPropsManagerOpen,
} from 'redux/actions/localActions';
import StrCss from './StrCss';
import DragCloseCardHeader from 'components/cards/generals/DragCloseCardHeader';
import TabPanel from 'components/TabPanel';

const PropsManagerCard = () => {
  const dispatch = useDispatch();
  const localState = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const {
    isDragging,
    isPropsManagerOpen,
    isBlockDragging,
    propsManagerTabIdx,
    page,
    project,
  } = localState;

  const classes = useStyles({ isPropsManagerOpen });

  return (
    <Draggable cancel={'[class*="MuiCardContent-root"]'}>
      <div className={classes.root}>
        <Fade in={isPropsManagerOpen && !isBlockDragging && !isDragging}>
          <Card elevation={5} className={classes.cardRoot}>
            <DragCloseCardHeader
              className={classes.header}
              onClickClose={() => {
                dispatch(setIsPropsManagerOpen(false));
              }}
            />
            <CardContent className={classes.content}>
              <>
                <AppBar position='sticky' color='default'>
                  <Tabs
                    value={propsManagerTabIdx}
                    onChange={(e, val) => dispatch(setPropsManagerTabIdx(val))}
                  >
                    <Tab label='Properties' className={classes.tabRoot} />
                    <Tab label='Layers' className={classes.tabRoot} />
                  </Tabs>
                </AppBar>
                <TabPanel value={propsManagerTabIdx} index={0}>
                  <div className={classes.contentWrapper}>
                    <PropsManager />
                    <Divider />
                    {project && page && <StrCss />}
                  </div>
                </TabPanel>
                <TabPanel value={propsManagerTabIdx} index={1}>
                  <div className={classes.layerWrapper}>
                    <div id='layerManager' />
                  </div>
                </TabPanel>
              </>
            </CardContent>
          </Card>
        </Fade>
      </div>
    </Draggable>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      position: 'absolute',
      top: 100,
      right: 20,
      zIndex: 1101,
      maxWidth: 300,
      minWidth: 200,
    },
    cardRoot: {
      width: (props: any) => (props.isPropsManagerOpen ? 'unset' : 0),
      height: (props: any) => (props.isPropsManagerOpen ? 'unset' : 0),
    },
    header: {
      backgroundColor: 'rgba(238,238,238,1)',
      cursor: 'move',
      paddingTop: 10,
      paddingBottom: 4,
    },
    contentWrapper: {
      padding: 24,
    },
    content: {
      maxHeight: '70vh',
      overflowY: 'auto',
      padding: 0,
    },
    layerWrapper: {
      paddingTop: 24,
    },
    tabRoot: {
      minWidth: 150,
    },
  })
);

export default PropsManagerCard;

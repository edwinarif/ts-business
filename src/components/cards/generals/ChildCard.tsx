import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Card, CardContent, Grow, AppBar, Tabs } from '@material-ui/core';
import DragCloseCardHeader from 'components/cards/generals/DragCloseCardHeader';
import TabPanel from 'components/TabPanel';
import { CardContentProps } from '@material-ui/core/CardContent';
import clsx from 'clsx';

type ChildCardType = {
  isOpen: boolean;
  onClickClose: () => void;
  cardContentProps?: CardContentProps;
};

const ChildCard: React.FC<ChildCardType> = ({
  isOpen,
  onClickClose,
  cardContentProps,
  ...props
}) => {
  const classes = useStyles();

  return (
    <Grow in={isOpen}>
      <Card elevation={5} className={classes.root}>
        <DragCloseCardHeader
          className={classes.header}
          onClickClose={onClickClose}
        />
        <CardContent
          className={clsx(
            cardContentProps ? cardContentProps.className : '',
            classes.cardContent
          )}
          {...cardContentProps}
        >
          <AppBar position='sticky' className={classes.appBar}>
            <Tabs value={0} onChange={() => {}}></Tabs>
          </AppBar>
          <TabPanel value={0} index={0}>
            {props.children}
          </TabPanel>
        </CardContent>
      </Card>
    </Grow>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      position: 'absolute',
      maxHeight: '70vh',
      maxWidth: 450,
      width: 450,
      marginLeft: 100,
      zIndex: 999,
      minWidth: 100,
      minHeight: 100,
    },
    appBar: {
      visibility: 'hidden',
      height: 0,
    },
    cardContent: {
      maxHeight: 'calc(70vh - 32px)',
      overflowY: 'auto',
      padding: 0,
    },
    header: {
      backgroundColor: '#f3f3f3',
      cursor: 'move',
      paddingTop: 10,
      paddingBottom: 4,
    },
  })
);

export default ChildCard;

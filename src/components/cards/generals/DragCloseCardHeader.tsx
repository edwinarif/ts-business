import React from 'react';
import {
  CardHeader,
  IconButton,
  makeStyles,
  createStyles,
} from '@material-ui/core';
import { CardHeaderProps } from '@material-ui/core/CardHeader';
import DragHandle from '@material-ui/icons/DragHandle';
import Close from '@material-ui/icons/Close';

const DragCloseCardHeader = (
  props: CardHeaderProps & { onClickClose?: () => void; hideClose?: boolean }
) => {
  const { onClickClose, hideClose, ...rest } = props;
  const classes = useStyles();

  return (
    <CardHeader
      action={
        <div className={classes.root}>
          <DragHandle className={classes.dragHandle} />
          {!hideClose && (
            <IconButton size='small' onClick={onClickClose}>
              <Close className={classes.closeIcon} />
            </IconButton>
          )}
        </div>
      }
      {...rest}
    />
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
    },
    dragHandle: {
      width: '0.8em',
      height: '0.8em',
      marginRight: 5,
    },
    closeIcon: {
      width: '0.8em',
      height: '0.8em',
    },
  })
);

export default DragCloseCardHeader;

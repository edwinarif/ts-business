import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { CardContent, Fab } from '@material-ui/core';
import Draggable from 'react-draggable';
import { setIsAddItemOpen, setIsAddPageOpen } from 'redux/actions/localActions';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import Card, { CardProps } from '@material-ui/core/Card';
import DragCloseCardHeader from 'components/cards/generals/DragCloseCardHeader';
import AddIcon from '@material-ui/icons/Add';
import PostAdd from '@material-ui/icons/PostAdd';
import AddPageCard from './AddPageCard';
import BlockAddItemCard from './BlockAddItemCard';

const MainNavCard = (props: CardProps) => {
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );
  const { isAddItemOpen, isBlockDragging, isDragging } = localReducer;

  const classes = useStyles({
    isAddItemOpen,
    isBlockDragging: isBlockDragging || isDragging,
  });
  const dispatch = useDispatch();

  const handleOpenCards = (cardKey: string) => () => {
    if (cardKey === 'addPage') {
      dispatch(setIsAddPageOpen(true));
      dispatch(setIsAddItemOpen(false));
    } else if (cardKey === 'addItem') {
      dispatch(setIsAddPageOpen(false));
      dispatch(setIsAddItemOpen(true));
    }
  };

  return (
    <Draggable cancel={'[class*="MuiCardContent-root"]'}>
      <div className={classes.mainWrapper}>
        <div className={classes.cardWrapper}>
          <Card elevation={5} className={classes.cardRoot} {...props}>
            <DragCloseCardHeader className={classes.cardHeader} hideClose />
            <CardContent>
              <Fab
                color='primary'
                aria-label='addpage'
                className={classes.fab}
                size='small'
                onClick={handleOpenCards('addPage')}
              >
                <PostAdd />
              </Fab>
              <Fab
                color='secondary'
                aria-label='additem'
                className={classes.fab}
                size='small'
                onClick={handleOpenCards('addItem')}
              >
                <AddIcon />
              </Fab>
            </CardContent>
          </Card>
        </div>
        <AddPageCard />
        <BlockAddItemCard />
      </div>
    </Draggable>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    mainWrapper: {
      position: 'fixed',
      zIndex: 1101,
      left: 36,
      top: 124,
    },
    cardWrapper: {
      display: (props: any) => (props.isBlockDragging ? 'none' : 'unset'),
    },
    cardRoot: {
      position: 'absolute',
      zIndex: 1101,
      opacity: (props: any) => (props.isAddItemOpen ? 1 : 0.99),
      '&:hover': {
        opacity: 1,
      },
    },
    cardHeader: {
      backgroundColor: '#f3f3f3',
      cursor: 'move',
      paddingTop: 10,
      paddingBottom: 4,
    },
    fab: {
      margin: theme.spacing(1),
    },
  })
);

export default MainNavCard;

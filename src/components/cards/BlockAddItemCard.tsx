import React, { useEffect } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Typography, Divider, Grid } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from 'index';
import {
  setIsAddItemOpen,
  addLoadingBlockId,
} from 'redux/actions/localActions';
import {
  defaultBlocks,
  BlockCategories,
  getBlockList,
  DefaultBlock,
} from 'componentsMapper';
import ChildCard from './generals/ChildCard';
import { useFirebaseConnect, useFirebase } from 'react-redux-firebase';
import BlockDrag from 'components/editorDnd/BlockDrag';
import { omitUndefined } from 'utils/mixins';

const BlockAddItemCard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const firebase = useFirebase();
  const { firebaseReducer, localReducer } = useSelector<AppState, AppState>(
    state => state
  );
  const {
    project,
    page,
    loadingBlockIds,
    currentProjectId,
    currentPageId,
  } = localReducer;

  const blockIds = page && page.blockIds ? Object.keys(page.blockIds) : [];
  useFirebaseConnect([
    `users/${firebaseReducer.auth.uid}/blocks`,
    ...blockIds.map(blockId => `blocks/${blockId}`),
  ]);
  const categories = Object.keys(defaultBlocks) as BlockCategories[];

  /**
   * Create Blocks if not in database
   */
  useEffect(() => {
    if (page && page.blockIds && firebaseReducer.data.blocks) {
      const loadedBlockIds = Object.keys(firebaseReducer.data.blocks || []);
      const nullLoadedBlockIds = loadedBlockIds.filter(
        id =>
          firebaseReducer.data.blocks![id] === null &&
          !loadingBlockIds.includes(id)
      );
      nullLoadedBlockIds.forEach(id => {
        dispatch(addLoadingBlockId(id));
      });

      if (nullLoadedBlockIds.length > 0) {
        const blockList = getBlockList() as {
          [key: string]: DefaultBlock;
        };
        nullLoadedBlockIds.forEach(blockId => {
          const block = blockList[blockId];
          const parseFirebaseBlock = omitUndefined(block);
          firebase.set(`blocks/${blockId}`, parseFirebaseBlock);
        });
      }
    }
  }, [page, firebaseReducer.data.blocks, firebase, dispatch, loadingBlockIds]);

  /**
   * Set 'page/blocks' ref once
   */
  useEffect(() => {
    if (project && page && !page.blockIds) {
      const blockList = getBlockList();
      const blockIds = Object.keys(blockList).reduce(
        (prev, blockId) => ({
          ...prev,
          [blockId]: true,
        }),
        {}
      );
      firebase.set(
        `projects/${currentProjectId}/pages/${currentPageId}/blockIds`,
        blockIds
      );
    }
  }, [project, page, firebase, currentProjectId, currentPageId]);

  return (
    <ChildCard
      isOpen={
        localReducer.isAddItemOpen &&
        !localReducer.isBlockDragging &&
        !localReducer.isDragging
      }
      onClickClose={() => dispatch(setIsAddItemOpen(false))}
    >
      <div className={classes.contentWrapper}>
        {categories.map(category => (
          <div key={`addItem-${category}`} className={classes.categoryRoot}>
            <Typography className={classes.categoryTitle}>
              {category}
            </Typography>
            <Grid container spacing={2}>
              {blockIds.map(blockId => {
                if (firebaseReducer.data.blocks) {
                  const block = firebaseReducer.data.blocks[blockId];
                  if (
                    page &&
                    block &&
                    block.category &&
                    block.category === category
                  ) {
                    return (
                      <Grid
                        item
                        key={`block-${blockId}`}
                        alignItems='center'
                        className={classes.blockGrid}
                      >
                        <div>
                          <BlockDrag
                            blockFirebase={block}
                            themeId={page.themeId}
                          />
                        </div>
                        <Typography
                          variant='caption'
                          gutterBottom
                          paragraph
                          align='center'
                          className={classes.blockLabel}
                        >
                          {block.label}
                        </Typography>
                      </Grid>
                    );
                  }
                }
                return null;
              })}
            </Grid>
            <Divider className={classes.divider} />
          </div>
        ))}
      </div>
    </ChildCard>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    contentWrapper: {
      padding: '12px 24px',
    },
    content: {
      display: 'flex',
    },
    categoryRoot: {},
    divider: {
      marginTop: 8,
      marginBottom: 16,
    },
    progress: {
      marginTop: 20,
    },
    categoryTitle: {
      marginBottom: 20,
    },
    blockGrid: {
      display: 'flex',
      flexDirection: 'column',
    },
    blockLabel: {
      marginTop: 8,
    },
  })
);

export default BlockAddItemCard;

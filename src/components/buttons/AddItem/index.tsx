import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Fab, { FabProps } from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

const AddItem = (props: FabProps) => {
  const classes = useStyles();

  return (
    <Fab
      color='primary'
      aria-label='add'
      className={classes.fab}
      size='small'
      {...props}
    >
      <AddIcon />
    </Fab>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    fab: {
      margin: theme.spacing(1),
    },
  })
);

export default AddItem;

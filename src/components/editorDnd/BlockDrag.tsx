import React, { useEffect } from 'react';
import { useDrag } from 'react-dnd';
import { createStyles, makeStyles } from '@material-ui/core';
import { useFirebase } from 'react-redux-firebase';
import { ComponentFirebaseType } from 'types';
import { parseComponent } from 'utils/parseComponent';
import { useSelector, useDispatch } from 'react-redux';
// @ts-ignore
import { Parser as HtmlParser } from 'html-to-react';
import { AppState } from 'index';
import { setIsBlockDragging, stopDragging } from 'redux/actions/localActions';
import clsx from 'clsx';

const BlockDrag = ({
  blockFirebase,
  themeId,
}: {
  blockFirebase: ComponentFirebaseType;
  themeId: string;
}) => {
  const dispatch = useDispatch();
  const firebase = useFirebase();
  const blocks = useSelector<AppState, any>(state => state.localReducer.blocks);
  const block = blocks[blockFirebase.id];

  useEffect(() => {
    parseComponent(dispatch, firebase, blockFirebase, themeId);
  }, [dispatch, firebase, themeId, blockFirebase]);

  const [{ isDragging }, drag] = useDrag({
    item: {
      id: blockFirebase.id,
      blockType: blockFirebase.blockType,
      type: 'box',
    },
    begin: () => {
      dispatch(setIsBlockDragging(true, blockFirebase.id));
    },
    end: () => {
      dispatch(stopDragging());
      dispatch(setIsBlockDragging(false, undefined));
    },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const classes = useStyles({ isDragging });

  if (!block) return null;

  const htmlParser = new HtmlParser();
  const element = htmlParser.parse(block.baseHtml);

  return (
    <>
      <element.type
        {...element.props}
        ref={drag}
        className={clsx(element.props.className, classes.root)}
        style={{ ...(block.blockStyleCss || {}) }}
      />
      <style>{block.baseCss}</style>
    </>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      minHeight: 36,
      minWidth: 36,
      padding: '0.5rem 1rem',
      cursor: 'move !important',
      opacity: ({ isDragging }: any) => (isDragging ? 0.4 : 1),
      '&:hover': {
        boxShadow: '2px 2px 5px 0px rgba(204,204,204,1)',
      },
    },
  })
);

export default BlockDrag;

import { AppState } from 'index';
import at from 'lodash/at';
import generatePropsFromDefault from 'utils/generatePropsFromDefault';

export const parseData = (state: AppState, currentPath: string) => {
  if (
    !currentPath ||
    !state.localReducer.project ||
    !state.localReducer.project.pages![state.localReducer.currentPageId]
  ) {
    return {
      componentsData: {
        page: undefined,
        componentStatus: [],
        themeConfig: undefined,
      },
    };
  }

  const page = state.localReducer.page!;

  const componentsBase = at<any>(page, `${currentPath}.components`);
  const componentIdsBase = at<any>(page, `${currentPath}.componentIds`);

  const components =
    componentsBase && componentsBase[0] ? componentsBase[0] : undefined;
  const componentIds =
    componentIdsBase && componentIdsBase[0] ? componentIdsBase[0] : undefined;

  const themeConfig = (state.localReducer.themes || {})[page.themeId];

  const componentsData = {
    page,
    themeConfig,
    localReducer: state.localReducer,
    componentStatus:
      page && components && componentIds
        ? (componentIds || []).map((componentId: string) => {
            const firebaseComponent = components[componentId];
            if (firebaseComponent === undefined) {
              return {
                componentsData: {
                  page: undefined,
                  componentStatus: [],
                  themeConfig: undefined,
                },
              };
            }

            const index = componentIds.indexOf(componentId);
            const blockId = at<any>(
              page,
              `${currentPath}.components.${componentId}.blockId`
            );

            return {
              componentId,
              index,
              blockFirebase:
                blockId.length > 0
                  ? (state.firebaseReducer.data.blocks || {})[blockId[0]]
                  : undefined,
              isSelected:
                componentId === state.localReducer.currentSelectedComponent,
              customProps: firebaseComponent.props
                ? generatePropsFromDefault(
                    firebaseComponent.blockType,
                    firebaseComponent.props
                  )
                : {},
              isComponentDragging:
                state.localReducer.dragComponentId === componentId,
              isHovered: state.localReducer.dragHoverIndex === index,
              isHoveredUp:
                state.localReducer.dragHoverIndex === index &&
                state.localReducer.dragDirection === 'up',
              isHoveredDown:
                state.localReducer.dragHoverIndex === index &&
                state.localReducer.dragDirection === 'down',
              components: firebaseComponent.components,
              componentIds: firebaseComponent.componentIds,
              strCss: firebaseComponent.strCss,
            };
          })
        : [],
  };

  return { componentsData };
};

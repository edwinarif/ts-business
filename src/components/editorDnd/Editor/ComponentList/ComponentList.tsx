import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AppState, database } from 'index';
import { useFirebase } from 'react-redux-firebase';
import ComponentDrag from './ComponentDrag';
import { handleDrop } from './ComponentDrag/handleDrop';
import { handleDropHover } from './ComponentDrag/handleDropHover';
import { handleDrag } from './ComponentDrag/handleDrag';
import { parseData } from './parseData';
import { setMerge } from 'redux/actions/localActions';

const ComponentList = ({ currentPath }: any) => {
  const firebase = useFirebase();
  const dispatch = useDispatch();
  const { componentsData } = useSelector<AppState, any>(state =>
    parseData(state, currentPath)
  );

  useEffect(() => {
    if ((componentsData.page.themeId, !componentsData.themeConfig)) {
      database
        .ref(`/themes/${componentsData.page.themeId}`)
        .once('value', snapshot => {
          dispatch(
            setMerge({
              key: 'themes',
              value: { [componentsData.page.themeId]: snapshot.val() },
            })
          );
        });
    }
  }, [componentsData.page.themeId, componentsData.themeConfig, dispatch]);

  return (
    <>
      {componentsData.themeConfig &&
        componentsData.componentStatus.map((data: any) =>
          data.blockFirebase &&
          ((data.components === undefined && data.componentIds === undefined) ||
            (data.components !== undefined &&
              data.componentIds !== undefined)) &&
          componentsData.themeConfig ? (
            <React.Fragment key={data.componentId}>
              <ComponentDrag
                themeConfig={componentsData.themeConfig.config}
                componentData={data}
                onDropHover={handleDropHover(
                  dispatch,
                  componentsData.localReducer
                )}
                onDragging={handleDrag(dispatch)}
                handleDrop={handleDrop(
                  dispatch,
                  firebase,
                  componentsData.localReducer,
                  componentsData.page
                )}
                dragHoverBound={componentsData.localReducer.dragHoverBound}
                currentPath={currentPath}
              />
            </React.Fragment>
          ) : null
        )}
    </>
  );
};

export default ComponentList;

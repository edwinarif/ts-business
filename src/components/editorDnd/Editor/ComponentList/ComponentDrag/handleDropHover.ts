import { DropTargetMonitor } from 'react-dnd';
import {
  setDropChildHover,
  setDragDirection,
} from 'redux/actions/localActions';
import { LocalReducerType } from 'redux/reducers/localReducer';

export const handleDropHover = (
  dispatch: any,
  localReducer: LocalReducerType
) => (ref: any, componentId: string, hoverIndex: number, path: string) => (
  item: any,
  monitor: DropTargetMonitor
) => {
  const isOverCurrent = monitor.isOver({ shallow: true });
  if (!isOverCurrent) return;
  if (!ref || !ref.current) return;
  if (
    (monitor.getDifferenceFromInitialOffset() as any).x === 0 &&
    (monitor.getDifferenceFromInitialOffset() as any).y === 0
  )
    return;

  const hoverBoundingRect = ref.current!.getBoundingClientRect() as (ClientRect &
    DOMRect);

  const clientOffset = monitor.getClientOffset() as any;
  const baseTreshold = hoverBoundingRect.width / 5;
  const treshold =
    baseTreshold >= 64
      ? 64
      : baseTreshold <= 8 && hoverBoundingRect.width > 24
      ? 8
      : baseTreshold;
  const leftTreshold = hoverBoundingRect.left + treshold;
  const rightTreshold = hoverBoundingRect.right - treshold;

  const direction =
    clientOffset.x <= leftTreshold
      ? 'down'
      : clientOffset.x >= rightTreshold
      ? 'up'
      : undefined;

  if (
    localReducer.dragHoverIndex === hoverIndex &&
    localReducer.dragDirection === direction
  )
    return;

  if (direction === undefined) {
    if (
      localReducer.dragHoverIsEditor === undefined &&
      localReducer.dragHoverIndex === hoverIndex &&
      localReducer.dragHoverPath === `${path}.components.${componentId}`
    )
      return;

    dispatch(
      setDropChildHover(
        hoverIndex,
        hoverBoundingRect,
        `${path}.components.${componentId}`
      )
    );
    return;
  }

  dispatch(
    setDragDirection(
      componentId,
      hoverIndex,
      direction,
      hoverBoundingRect,
      undefined,
      path
    )
  );
  return;
};

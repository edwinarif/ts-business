import React from 'react';
import { ComponentFirebaseType, NestedComponentType } from 'types';
import { useFirebase } from 'react-redux-firebase';
import { useDispatch } from 'react-redux';
import {
  setCurrentSelectedComponent,
  setIsPropsManagerOpen,
} from 'redux/actions/localActions';
// @ts-ignore
import { Parser as HtmlParser } from 'html-to-react';
import { parseComponent } from 'utils/parseComponent';
import isEqual from 'lodash/isEqual';
import { ComponentProps } from 'componentsMapper';
import ComponentList from '..';
import isValidCss from 'utils/isValidCss';
import { CSSProperties } from '@material-ui/styles';

type ComponentElementType = {
  componentId: string;
  themeConfig: string;
  blockFirebase: ComponentFirebaseType;
  isSelected: boolean;
  isComponentDragging: boolean;
  isHoveredDrop: boolean;
  customProps: ComponentProps;
  strCss?: string;
  baseRef: React.RefObject<HTMLElement>;
  nestedComponent: NestedComponentType;
  currentPath: string;
};

const ComponentElement = React.memo(
  ({
    componentId,
    themeConfig,
    blockFirebase,
    isSelected,
    isComponentDragging,
    isHoveredDrop,
    customProps,
    strCss,
    baseRef,
    nestedComponent,
    currentPath,
  }: ComponentElementType) => {
    const dispatch = useDispatch();
    const firebase = useFirebase();

    /** Get Html & Css */
    const component = parseComponent(
      dispatch,
      firebase,
      blockFirebase,
      undefined,
      componentId,
      customProps,
      themeConfig
    );

    /**
     * localReducer isDragging as a source of truth,
     * set true here, and set false in end drag function
     */

    if (!component) return null;
    if (!blockFirebase) return null;

    const htmlParser = new HtmlParser();
    const element = htmlParser.parse(component.baseHtml);

    const handleClick = (e: React.MouseEvent<any, MouseEvent>) => {
      e.stopPropagation();
      dispatch(
        setCurrentSelectedComponent(
          componentId,
          `${currentPath}.components.${componentId}`
        )
      );
      dispatch(setIsPropsManagerOpen(true));
    };

    return (
      <>
        {/* {console.log('rerender element', componentId, nestedComponent, element)} */}
        {nestedComponent &&
        nestedComponent.components &&
        nestedComponent.componentIds &&
        nestedComponent.componentIds.length > 0 ? (
          <element.type
            {...element.props}
            id={componentId}
            ref={baseRef}
            style={getStyle(
              isSelected,
              isComponentDragging,
              isHoveredDrop,
              component.blockStyleCss || {}
            )}
            onClick={handleClick}
          >
            {/* {console.log('render element with componentList', nestedComponent)} */}
            <ComponentList
              currentPath={`${currentPath}.components.${componentId}`}
            />
          </element.type>
        ) : (
          <>
            {/* {console.log('masuk sini lah', componentId, nestedComponent)} */}
            <element.type
              {...element.props}
              id={componentId}
              ref={baseRef}
              style={getStyle(
                isSelected,
                isComponentDragging,
                isHoveredDrop,
                component.blockStyleCss || {}
              )}
              onClick={handleClick}
            />
          </>
        )}
        <style>{component.baseCss}</style>
        {strCss && isValidCss(strCss) && (
          <style>{`#${componentId}{${strCss}}`}</style>
        )}
      </>
    );
  },
  (prevProps, nextProps) => {
    return (
      prevProps.isComponentDragging === nextProps.isComponentDragging &&
      prevProps.isSelected === nextProps.isSelected &&
      prevProps.isHoveredDrop === nextProps.isHoveredDrop &&
      prevProps.themeConfig === nextProps.themeConfig &&
      prevProps.strCss === nextProps.strCss &&
      (prevProps.nestedComponent.componentIds || []).length ===
        (nextProps.nestedComponent.componentIds || []).length &&
      isEqual(prevProps.blockFirebase, nextProps.blockFirebase) &&
      isEqual(prevProps.customProps, nextProps.customProps) &&
      isEqual(
        prevProps.nestedComponent.components || {},
        nextProps.nestedComponent.components || {}
      )
    );
  }
);

const getStyle = (
  isComponentSelected: boolean,
  isComponentDragging: boolean,
  isHoveredDrop: boolean,
  blockStyleCss: CSSProperties
) => ({
  ...blockStyleCss,
  minHeight: 32,
  opacity: (() => {
    if (isComponentDragging) return 0.4;
    return 1;
  })(),
  cursor: 'move',
  outline: (() => {
    if (isComponentDragging) return '2px dashed black';
    if (isHoveredDrop) return '3px solid rgba(255,193,7,0.7)';
    if (isComponentSelected) return '3px solid rgba(100,181,246,1)';
    return blockStyleCss && blockStyleCss.outline
      ? blockStyleCss.outline
      : undefined;
  })(),
  '&:hover': {
    outline: (() => {
      if (isComponentSelected) return '3px solid rgba(100,181,246,1)';
      return '3px solid rgba(224,224,224,1)';
    })(),
  },
});

export default ComponentElement;

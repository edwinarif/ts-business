import React, { useRef, useEffect } from 'react';
// @ts-ignore
import { makeStyles, createStyles, Fab } from '@material-ui/core';
import clsx from 'clsx';
import ComponentElement from './ComponentElement';
import { useDrop, useDrag } from 'react-dnd';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { handleDropHoverBase } from '../../handleDropHoverBase';
import Popper from 'popper.js';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import { useFirebase } from 'react-redux-firebase';
import at from 'lodash/at';

type ComponentDragType = {
  themeConfig: string;
  componentData: any;
  onDropHover: any;
  onDragging: any;
  handleDrop: any;
  dragHoverBound: any;
  currentPath: string;
};

const ComponentDrag = ({
  componentData,
  themeConfig,
  onDropHover,
  onDragging,
  handleDrop,
  dragHoverBound,
  currentPath,
}: ComponentDragType) => {
  const firebase = useFirebase();
  const dispatch = useDispatch();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );

  const { currentProjectId, currentPageId } = localReducer;

  const {
    componentId,
    blockFirebase,
    index,
    isSelected,
    isComponentDragging,
    isHovered,
    isHoveredUp,
    isHoveredDown,
    customProps,
    components,
    componentIds,
    strCss,
  } = componentData;

  const classes = useStyles({
    bound: dragHoverBound,
    isHoveredDown,
    isSelected,
    isComponentDragging,
  });

  const ref = useRef<HTMLElement>(null);
  const popperRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    let popper: Popper | undefined;
    if (ref && popperRef && ref.current && popperRef.current) {
      popper = new Popper(ref.current, popperRef.current, {
        placement: 'bottom-end',
      });
    }
    return function cleanup() {
      if (popper) popper.destroy();
    };
  }, [ref, popperRef, localReducer.project]);

  const [{ isOverCurrent }, drop] = useDrop({
    accept: blockFirebase.acceptDrop || '',
    hover: (item, monitor) => {
      if (!components) {
        onDropHover(ref, componentId, index, currentPath)(item, monitor);
        return;
      }

      handleDropHoverBase(
        dispatch,
        localReducer,
        componentId,
        `${currentPath}.components.${componentId}`
      )(item, monitor);
      return;
    },
    drop: handleDrop,
    collect: monitor => ({
      isOverCurrent: monitor.isOver({ shallow: true }),
    }),
  });

  const [, drag] = useDrag({
    item: {
      id: componentId,
      name: componentId,
      type: 'box',
    },
    collect: monitor => {
      return {
        isDragging: monitor.isDragging(),
      };
    },
    begin: onDragging(ref, componentId, true, index, currentPath),
  });
  drag(drop(ref));

  const handleDelete = () => {
    const componentIds = at<any>(
      localReducer.project,
      `pages.${currentPageId}.${currentPath}.componentIds`
    )[0];
    firebase
      .ref(
        `projects/${currentProjectId}/pages/${currentPageId}/${currentPath.replace(
          /\./g,
          '/'
        )}`
      )
      .update({
        componentIds: componentIds.filter((id: any) => id !== componentId),
        [`components/${componentId}`]: null,
      });
  };

  return (
    <>
      {isHovered && isHoveredDown && (
        <div className={clsx(classes.divHovered, 'hoveredPointer')} />
      )}
      {isOverCurrent && !isHoveredUp && !isHoveredDown && !components && (
        <div className={classes.dropChildPointer} />
      )}
      <ComponentElement
        componentId={componentId}
        themeConfig={themeConfig}
        blockFirebase={blockFirebase}
        isSelected={isSelected}
        isComponentDragging={isComponentDragging}
        isHoveredDrop={isOverCurrent && !isHoveredUp && !isHoveredDown}
        customProps={customProps}
        baseRef={ref}
        nestedComponent={{ components, componentIds }}
        currentPath={currentPath}
        strCss={strCss}
      />
      <div ref={popperRef} className={classes.popper}>
        <Fab
          color='default'
          aria-label='delete'
          size='small'
          className={classes.popperFab}
          onClick={handleDelete}
        >
          <DeleteIcon className={classes.popperFabIcon} />
        </Fab>
        <Fab
          color='default'
          aria-label='select parent'
          size='small'
          className={classes.popperFab}
        >
          <ArrowUpwardIcon className={classes.popperFabIcon} />
        </Fab>
      </div>
      {isHovered && isHoveredUp && (
        <div className={clsx(classes.divHovered, 'hoveredPointer')} />
      )}
    </>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      minHeight: 32,
    },
    divHovered: {
      position: 'fixed',
      zIndex: 999,
      top: ({ bound }: any) => (bound ? bound.top : 100),
      left: ({ bound, isHoveredDown }: any) =>
        bound ? (isHoveredDown ? bound.left : bound.right) : undefined,
      width: 0,
      height: ({ bound }: any) => (bound ? bound.height : 12),
      minHeight: 12,
      boxShadow: '0px 0px 0px 4px rgba(255,193,7,0.7)',
    },
    dropChildPointer: {
      position: 'fixed',
      zIndex: 999,
      top: ({ bound }: any) =>
        bound ? bound.top + bound.height / 2 : undefined,
      width: ({ bound }: any) =>
        bound ? bound.width - bound.width / 8 : undefined,
      left: ({ bound }: any) => (bound ? bound.width / 16 : undefined),
      height: 0,
      boxShadow: '0px 0px 0px 2px rgba(255,193,7,0.2)',
    },
    popper: {
      visibility: ({ isSelected, isComponentDragging }: any) =>
        isSelected && !isComponentDragging ? 'visible' : 'hidden',
      zIndex: 999,
      marginTop: 10,
    },
    popperFab: {
      marginLeft: 8,
      width: 32,
      height: 32,
      minWidth: 'unset',
      minHeight: 'unset',
    },
    popperFabIcon: {
      width: 16,
      height: 16,
      minWidth: 'unset',
      minHeight: 'unset',
    },
  })
);

export default ComponentDrag;

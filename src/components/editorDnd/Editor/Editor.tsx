import React, { useEffect } from 'react';
import { useDrop } from 'react-dnd';
import { Typography, createStyles, makeStyles } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from 'index';
import {
  setCurrentSelectedComponent,
  setIsPropsManagerOpen,
  setDragDirection,
} from 'redux/actions/localActions';
import { useFirebase } from 'react-redux-firebase';
import ComponentList from './ComponentList';
import { handleDropHoverBase } from './handleDropHoverBase';
import { handleDropEditor } from './handleDropEditor';
import { LocalReducerType } from 'redux/reducers/localReducer';

const Editor = () => {
  const firebase = useFirebase();
  const dispatch = useDispatch();
  const localReducer = useSelector<AppState, LocalReducerType>(
    state => state.localReducer
  );

  const {
    currentSelectedComponent,
    currentProjectId,
    currentPageId,
    dragDirection,
    project,
    page,
  } = localReducer;

  const components = ((page || {}).rootComponent || {}).components;
  const pageComponentIds = ((page || {}).rootComponent || {}).componentIds;

  useEffect(() => {
    if (components && !pageComponentIds) {
      firebase.set(
        `projects/${currentProjectId}/pages/${currentPageId}/rootComponent/componentIds`,
        Object.keys(components)
      );
    }
  }, [
    firebase,
    components,
    currentPageId,
    currentProjectId,
    pageComponentIds,
    dispatch,
  ]);

  const isEditorSelected = currentSelectedComponent === 'rootComponent';

  const [{ canDrop, isOver }, drop] = useDrop({
    accept: 'box',
    drop: handleDropEditor(firebase, dispatch, localReducer, page!),
    hover: (item, monitor) => {
      if (components) {
        handleDropHoverBase(
          dispatch,
          localReducer,
          'rootComponent',
          'rootComponent'
        )(item, monitor);
        return;
      }

      if (dragDirection !== 'up') {
        dispatch(
          setDragDirection(
            'rootComponent',
            0,
            'up',
            undefined,
            true,
            'rootComponent'
          )
        );
      }
    },
    collect: monitor => ({
      canDrop: monitor.canDrop(),
      isOver: monitor.isOver(),
      isOverCurrent: monitor.isOver({ shallow: true }),
    }),
  });

  const isActive = canDrop && isOver;
  const classes = useStyles({
    isActive,
    canDrop,
    isEditorSelected,
    isComponentsAvailable: pageComponentIds,
  });

  const handleClick = () => {
    dispatch(setCurrentSelectedComponent('rootComponent', 'rootComponent'));
    dispatch(setIsPropsManagerOpen(false));
  };

  const rootComponent = project!.pages![currentPageId].rootComponent || {};

  return (
    <div
      id='rootComponent'
      ref={drop}
      className={classes.root}
      onClick={handleClick}
    >
      {!pageComponentIds && (
        <Typography variant='h4' className={classes.emptyTitle}>
          {isActive ? 'Release to drop' : 'Drag a component here'}
        </Typography>
      )}
      {rootComponent.components && (
        <ComponentList currentPath={'rootComponent'} />
      )}
    </div>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      height: 'calc(100vh - 108px)',
      width: '100%',
      color: 'black',
      backgroundSize: '24px 24px',
      backgroundColor: 'rgba(238,238,238,1)',
      backgroundImage:
        'radial-gradient(circle, rgba(189,189,189,1) 1px, rgba(0, 0, 0, 0) 1px)',
      border: ({ isActive, canDrop, isEditorSelected }: any) => {
        if (isActive) return '3px solid rgba(0,150,136,1)';
        if (canDrop) return '3px solid rgba(255,167,38,1)';
        if (isEditorSelected) return '3px solid rgba(100,181,246,1)';
        return '3px solid transparent';
      },
      display: ({ isComponentsAvailable }) => {
        return isComponentsAvailable ? 'block' : 'flex';
      },
      justifyContent: 'center',
      alignItems: 'center',
      '&:hover': {
        border: ({ isEditorSelected }: any) => {
          if (isEditorSelected) return '3px solid rgba(100,181,246,1)';
          return '3px solid rgba(224,224,224,1)';
        },
      },
    },
    emptyTitle: {
      color: 'rgba(224,224,224,1)',
    },
  })
);

export default Editor;

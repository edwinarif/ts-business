import { LocalReducerType } from 'redux/reducers/localReducer';
import { DropTargetMonitor } from 'react-dnd';
import { Dispatch } from 'redux';
import { dropMove } from 'components/editorDnd/Editor/dropMove/dropMove';
import { ExtendedFirebaseInstance } from 'react-redux-firebase';
import { IPage } from 'types';
import { dropFromBlock } from './dropMove/dropFromBlock';
import { stopDragging } from 'redux/actions/localActions';

export const handleDropEditor = (
  firebase: ExtendedFirebaseInstance,
  dispatch: Dispatch<any>,
  localReducer: LocalReducerType,
  page: IPage
) => (item: any, monitor: DropTargetMonitor) => {
  const isOverCurrent = monitor.isOver({ shallow: true });
  if (!isOverCurrent) return;

  if (!localReducer.isBlockDragging && localReducer.dragHoverIsEditor) {
    // console.log('component moved');
    dropMove(dispatch, firebase, page, localReducer);
    dispatch(stopDragging());
    return;
  }

  if (localReducer.isBlockDragging && localReducer.dragHoverIsEditor) {
    // console.log('component insert');
    dropFromBlock(firebase, page, localReducer);
    dispatch(stopDragging());
    return;
  }
};

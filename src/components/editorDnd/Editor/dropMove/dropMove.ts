import { move, insert } from 'utils/mixins';
import { ExtendedFirebaseInstance } from 'react-redux-firebase';
import { stopDragging } from 'redux/actions/localActions';
import { LocalReducerType } from 'redux/reducers/localReducer';
import at from 'lodash/at';
import { IPage } from 'types';

export function dropMove(
  disptach: any,
  firebase: ExtendedFirebaseInstance,
  page: IPage,
  localReducer: LocalReducerType
): void {
  const {
    dragComponentId,
    dragIndex,
    dragPath,
    dragHoverComponentId,
    dragHoverIndex,
    dragHoverPath,
    dragDirection,
    currentProjectId,
    currentPageId,
  } = localReducer;

  if (
    dragComponentId === undefined ||
    dragPath === undefined ||
    dragIndex === undefined ||
    dragHoverIndex === undefined ||
    dragHoverPath === undefined ||
    (dragPath === dragHoverPath && dragIndex === dragHoverIndex) ||
    (dragPath === dragHoverPath && dragDirection === undefined) ||
    (dragPath === dragHoverPath &&
      dragDirection === 'down' &&
      dragHoverIndex - dragIndex === 1) ||
    (dragPath === dragHoverPath &&
      dragDirection === 'up' &&
      dragHoverIndex - dragIndex === -1)
  ) {
    // console.log('cancel drop');
    disptach(stopDragging());
    return;
  }

  const dragComponent = at<any>(
    page,
    `${dragPath}.components.${dragComponentId}`
  )[0];

  const dragPathRef = `projects/${currentProjectId}/pages/${currentPageId}/${dragPath.replace(
    /\./g,
    '/'
  )}`;

  const getToIndex = () => {
    if (dragHoverIndex > dragIndex)
      return dragDirection === 'down' ? dragHoverIndex - 1 : dragHoverIndex;

    return dragDirection === 'down' ? dragHoverIndex : dragHoverIndex + 1;
  };

  // move same level
  if (dragPath === dragHoverPath) {
    // console.log('move same level');
    const componentIds = at<any>(
      page,
      `${dragPath}.componentIds`
    )[0] as string[];
    const movedComponents = move(componentIds, dragIndex, getToIndex());
    firebase.ref(`${dragPathRef}/componentIds`).set(movedComponents);
    return;
  }

  // move different level
  if (dragPath !== dragHoverPath) {
    // console.log('move different level');
    const hoverPathRef = `projects/${currentProjectId}/pages/${currentPageId}/${dragHoverPath.replace(
      /\./g,
      '/'
    )}`;
    const dragPathData = at<any>(page, dragPath)[0];
    const hoverPathData = at<any>(page, dragHoverPath)[0];

    const filteredComponentIds = (() => {
      if (!dragPathData || !dragPathData.componentIds) return null;

      const filteredComponentIds = dragPathData.componentIds.filter(
        (id: any) => id !== dragComponentId
      );
      if (filteredComponentIds.length === 0) return null;
      return filteredComponentIds;
    })();

    const addedComponentIds = (() => {
      if (!hoverPathData || !hoverPathData.componentIds)
        return [dragComponentId];

      const hoverIds = hoverPathData.componentIds || [];
      const hoverIdx = hoverIds.indexOf(dragHoverComponentId);
      const directionIdx = dragDirection === 'down' ? hoverIdx : hoverIdx + 1;
      const added = insert(
        hoverPathData.componentIds,
        directionIdx,
        dragComponentId
      );
      return added;
    })();

    firebase.ref(dragPathRef).update({
      componentIds: filteredComponentIds,
      [`components/${dragComponentId}`]: null,
    });
    firebase.ref(hoverPathRef).update({
      componentIds: addedComponentIds,
      [`components/${dragComponentId}`]: dragComponent,
    });

    // console.log('dragComponent', dragComponent);
    return;
  }
}

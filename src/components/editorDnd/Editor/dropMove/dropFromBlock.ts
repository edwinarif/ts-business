import { IPage } from 'types';
import { insert } from 'utils/mixins';
import { ExtendedFirebaseInstance } from 'react-redux-firebase';
import { LocalReducerType } from 'redux/reducers/localReducer';
import at from 'lodash/at';

export const dropFromBlock = (
  firebase: ExtendedFirebaseInstance,
  page: IPage,
  localReducer: LocalReducerType
) => {
  const {
    currentProjectId,
    currentPageId,
    blocks,
    dragHoverPath,
    dragBlockId,
    isBlockDragging,
  } = localReducer;

  if (!isBlockDragging || !dragBlockId || !dragHoverPath) return;
  // console.log('insert');

  const block = blocks[dragBlockId];
  const firebaseRefPath = `projects/${currentProjectId}/pages/${currentPageId}/${dragHoverPath.replace(
    /\./g,
    '/'
  )}/components`;

  firebase
    .ref(firebaseRefPath)
    .push(
      block.props
        ? {
            blockId: block.id,
            blockType: block.blockType,
            props: block.props,
          }
        : {
            blockId: block.id,
            blockType: block.blockType,
          }
    )
    .then(ref => {
      if (ref.key) {
        const getAtComponentIds = at<any>(
          page,
          `${dragHoverPath}.componentIds`
        );
        firebase.set(
          `projects/${currentProjectId}/pages/${currentPageId}/${dragHoverPath.replace(
            /\./g,
            '/'
          )}/componentIds`,
          insert<string>(
            getAtComponentIds.length > 0 ? getAtComponentIds[0] : [],
            localReducer.dragDirection === 'down'
              ? localReducer.dragHoverIndex!
              : localReducer.dragHoverIndex! + 1,
            ref.key
          )
        );
      }
    });
};

import React, { useState } from 'react';
import { firebaseAuth, AppState } from 'index';
import FirebaseLoginDialog from 'components/dialogs/FirebaseLoginDialog';
import GetCodeDialog from 'components/dialogs/GetCodeDialog';
import { useSelector, useDispatch } from 'react-redux';
import templateEngine from 'templates/templateEngine';
import clsx from 'clsx';
import {
  AppBar,
  Toolbar,
  Button,
  makeStyles,
  createStyles,
  Link,
  Theme,
} from '@material-ui/core';
import { useRouter } from 'components/HookedBrowserRouter';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { FirebaseState } from 'types';
import { setIsAddPageOpen } from 'redux/actions/localActions';
import DownIcon from '@material-ui/icons/KeyboardArrowDown';

const DefaultNavbar = ({ className }: { className?: string }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useRouter();
  const [isFirebaseUiOpen, setIsFirebaseUiOpen] = useState(false);
  const [isGetCodeDialogOpen, setIsGetCodeDialogOpen] = useState(false);
  const [codeValue, setCodeValue] = useState('');
  const firebaseReducer = useSelector<AppState, FirebaseState>(
    state => state.firebaseReducer as FirebaseState
  );
  const user = firebaseReducer.auth;
  const { page, project, currentProjectId } = useSelector<
    AppState,
    LocalReducerType
  >(state => state.localReducer);

  const handleLogin = () => {
    if (user.isEmpty) {
      setIsFirebaseUiOpen(true);
    } else {
      setIsFirebaseUiOpen(false);
    }
  };

  const handleLogout = () => {
    firebaseAuth.signOut();
    setIsFirebaseUiOpen(false);
  };

  const handleGetCode = () => {
    if (
      project &&
      page &&
      page.rootComponent &&
      page.rootComponent.componentIds &&
      page.rootComponent.components
    ) {
      setCodeValue(
        templateEngine(
          page.rootComponent.componentIds,
          page.rootComponent.components
        )
      );
    }
    setIsGetCodeDialogOpen(true);
  };

  const goTo = (route: string) => () => {
    router!.history.push(route);
  };

  const handleChangePage = () => {
    dispatch(setIsAddPageOpen(true));
  };

  const handleChangeProject = () => {
    router!.history.push('/projects');
  };

  return (
    <>
      <AppBar
        position='fixed'
        color='inherit'
        elevation={0}
        className={clsx(className, classes.appbar)}
      >
        <Toolbar variant='dense'>
          <Link onClick={goTo('/')} color='primary' className={classes.title}>
            REPLATES
          </Link>
          {!user.isEmpty && router!.location.pathname !== '/dashboard' && (
            <>
              <Link
                onClick={goTo('/projects')}
                color='secondary'
                className={classes.link}
              >
                Projects
              </Link>
              {currentProjectId && (
                <Link
                  onClick={goTo('/dashboard')}
                  color='secondary'
                  className={classes.link}
                >
                  Dashboard
                </Link>
              )}
            </>
          )}
          {!user.isEmpty &&
            router!.location.pathname === '/dashboard' &&
            project && (
              <Button
                className={classes.statusButton}
                disableFocusRipple
                onClick={handleChangeProject}
              >
                Project:
                <span className={classes.statusButtonText}>{project.name}</span>
                <DownIcon />
              </Button>
            )}
          {!user.isEmpty && router!.location.pathname === '/dashboard' && page && (
            <Button
              className={classes.statusButton}
              disableFocusRipple
              onClick={handleChangePage}
            >
              Page:
              <span className={classes.statusButtonText}>{page.name}</span>
              <DownIcon />
            </Button>
          )}

          <div className={classes.rightWrapper}>
            {user.isEmpty && !firebaseReducer.isInitializing && (
              <Button
                color='inherit'
                size='small'
                onClick={handleLogin}
                className={classes.button2}
              >
                Login
              </Button>
            )}
            {!user.isEmpty && project && (
              <>
                <Button
                  color='primary'
                  variant='contained'
                  size='small'
                  onClick={handleGetCode}
                  className={classes.button}
                >
                  Get Code
                </Button>
              </>
            )}
            {!user.isEmpty && (
              <Button
                color='inherit'
                size='small'
                onClick={handleLogout}
                className={classes.button2}
              >
                Logout
              </Button>
            )}
          </div>
        </Toolbar>
        {!user.isEmpty && (
          <Toolbar variant='dense' className={classes.secondToolbar}></Toolbar>
        )}
      </AppBar>
      {user.isEmpty && (
        <FirebaseLoginDialog
          isOpen={isFirebaseUiOpen}
          handleClose={() => setIsFirebaseUiOpen(false)}
        />
      )}
      <GetCodeDialog
        isOpen={isGetCodeDialogOpen}
        handleClose={() => setIsGetCodeDialogOpen(false)}
        codeValue={codeValue}
        isLoading={false}
      />
    </>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appbar: {},
    secondToolbar: {
      borderBottom: '1px solid rgba(189,189,189,1)',
      borderTop: '1px solid rgba(189,189,189,1)',
      backgroundColor: 'rgba(33,150,243,1)',
      minHeight: 36,
    },
    secondToolbarText: {
      marginRight: 20,
    },
    title: {
      cursor: 'pointer',
      fontFamily: "'Abril Fatface', serif",
      letterSpacing: 3,
      marginRight: 40,
      fontSize: '1.7em',
      '&:hover': {
        textDecoration: 'unset',
      },
    },
    link: {
      cursor: 'pointer',
      marginRight: 30,
      '&:hover': {
        textDecoration: 'unset',
      },
    },
    rightWrapper: {
      display: 'flex',
      flexGrow: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    button: {
      marginRight: 10,
      paddingLeft: '20px !important',
      paddingRight: '20px !important',
    },
    button2: {
      marginLeft: 10,
      paddingLeft: '15px !important',
      paddingRight: '15px !important',
    },
    statusButton: {
      fontSize: 13,
      marginRight: 20,
    },
    statusButtonText: {
      fontSize: '13px !important',
      marginLeft: 10,
      marginRight: 5,
      color: theme.palette.primary.main,
    },
  })
);

export default DefaultNavbar;

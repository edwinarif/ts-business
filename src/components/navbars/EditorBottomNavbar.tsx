import React from 'react';
import {
  AppBar,
  Typography,
  Toolbar,
  createStyles,
  makeStyles,
} from '@material-ui/core';

const EditorBottomNavbar = () => {
  const classes = useStyles();

  return (
    <AppBar position='fixed' color='secondary' className={classes.appBar}>
      <Toolbar className={classes.toolBar}>
        <Typography variant='caption'></Typography>
      </Toolbar>
    </AppBar>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    appBar: {
      top: 'auto',
      bottom: 0,
    },
    toolBar: {
      borderTop: '1px solid rgba(189,189,189,1)',
      minHeight: 24,
    },
  })
);

export default EditorBottomNavbar;

import React, { useEffect } from 'react';
import { makeStyles, createStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';
import DefaultNavbar from 'components/navbars/DefaultNavbar';
import './styles.css';
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentPage, setKeyValue } from 'redux/actions/localActions';
import PropsManagerCard from 'components/cards/PropsManagerCard';
import MainNavCard from 'components/cards/MainNavCard';
import EditorBottomNavbar from 'components/navbars/EditorBottomNavbar';
import { AppState, database } from 'index';
import { LocalReducerType } from 'redux/reducers/localReducer';
import { useFirebase } from 'react-redux-firebase';
import { IPage } from 'types';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import Editor from 'components/editorDnd/Editor';
import at from 'lodash/at';

const EditorPage = () => {
  const firebase = useFirebase();
  const classes = useStyles();
  const dispatch = useDispatch();
  const { currentProjectId, currentPageId, project, page } = useSelector<
    AppState,
    LocalReducerType
  >(state => state.localReducer);

  if (!currentProjectId) {
    window.location.href = '/projects';
  }

  useEffect(() => {
    if (currentProjectId) {
      database.ref(`projects/${currentProjectId}`).on('value', snapshot => {
        dispatch(setKeyValue({ key: 'project', value: snapshot.val() }));
      });
    }

    return function cleanup() {
      if (currentProjectId) {
        database.ref(`projects/${currentProjectId}`).off('value');
      }
    };
  }, [dispatch, currentProjectId]);

  useEffect(() => {
    if (project && project.name && !project.pages) {
      const page: IPage = {
        name: 'Home',
        themeId: project.defaultThemeId || 'theme1',
        createdAt: Date.now(),
        updatedAt: Date.now(),
      };
      firebase.push(`projects/${currentProjectId}/pages/`, page).then(res => {
        if (res.key) dispatch(setCurrentPage(res.key, page));
      });
    }
  }, [dispatch, firebase, project, currentProjectId]);

  useEffect(() => {
    if (currentPageId) {
      const page = at<any>(project, `pages.${currentPageId}`)[0];
      dispatch(setCurrentPage(currentPageId, page));
    }
  }, [dispatch, project, currentPageId]);

  return (
    <DndProvider backend={HTML5Backend}>
      <div className={classes.root}>
        <DefaultNavbar />
        <main className={classes.content}>
          {project && project.name && page && (
            <>
              <MainNavCard />
              <Editor />
            </>
          )}
        </main>
      </div>
      <PropsManagerCard />
      <EditorBottomNavbar />
    </DndProvider>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      marginTop: 84,
      minHeight: 'calc(100vh - 84px)',
    },
  })
);

export default EditorPage;

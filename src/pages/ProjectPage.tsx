import React, { useState } from 'react';
import DefaultNavbar from 'components/navbars/DefaultNavbar';
import { makeStyles, createStyles } from '@material-ui/styles';
import {
  Button,
  Typography,
  Dialog,
  DialogContent,
  TextField,
  DialogActions,
  Grid,
  Container,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from 'index';
import { useFirebase, useFirebaseConnect } from 'react-redux-firebase';
import { setCurrentProject } from 'redux/actions/localActions';
import { useRouter } from 'components/HookedBrowserRouter';
import { FirebaseState, IProject } from 'types';

const ProjectPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useRouter();

  const mainState = useSelector<AppState, any>(state => ({
    auth: (state.firebaseReducer as FirebaseState).auth,
    users: (state.firebaseReducer as FirebaseState).data.users,
  }));

  const firebase = useFirebase();
  useFirebaseConnect([`users/${mainState.auth.uid}/projects`]);

  const [projectName, setProjectName] = useState('');
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const handleCancel = () => {
    setIsDialogOpen(false);
    setProjectName('');
  };

  const handleCreate = () => {
    const name = projectName.trim();
    if (name !== '') {
      const dateNow = Date.now();
      const newProject: IProject = {
        name,
        ownerId: mainState.auth.uid,
        defaultThemeId: 'theme1',
        createdAt: dateNow,
        updatedAt: dateNow,
      };
      firebase.push(`projects`, newProject).then((res: any) => {
        if (res.key) {
          firebase
            .set(`users/${mainState.auth.uid}/projects/${res.key}`, {
              name,
              createdAt: dateNow,
            })
            .then(() => {
              dispatch(setCurrentProject(res.key, newProject));
              router!.history.push('/dashboard');
            });
        }
      });
      handleCancel();
    }
  };

  const handleClickProject = (projectId: string) => () => {
    firebase.ref(`/projects/${projectId}`).once('value', snapshot => {
      dispatch(setCurrentProject(projectId, snapshot.val()));
      router!.history.push('/dashboard');
    });
  };

  return (
    <div className={classes.root}>
      <DefaultNavbar />
      <main className={classes.content}>
        <Container>
          <Grid container spacing={4}>
            <Grid item>
              <Button
                onClick={() => {
                  setIsDialogOpen(true);
                }}
                className={classes.addProject}
                variant='contained'
                disableFocusRipple
                color='primary'
              >
                <div>
                  <AddIcon className={classes.addIcon} />
                  <Typography>Add Project</Typography>
                </div>
              </Button>
            </Grid>
            {Object.keys(
              ((mainState.users || {})[mainState.auth.uid] || {}).projects || {}
            ).map(id => (
              <Grid item key={`project-${id}`}>
                <Button
                  className={classes.addProject}
                  variant='contained'
                  disableFocusRipple
                  color='default'
                  onClick={handleClickProject(id)}
                >
                  <div>
                    {mainState.users[mainState.auth.uid].projects[id].name}
                  </div>
                </Button>
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
      <Dialog
        open={isDialogOpen}
        onClose={handleCancel}
        aria-labelledby='form-dialog-title'
      >
        <DialogContent>
          <TextField
            autoFocus
            margin='dense'
            id='name'
            label='Project name'
            value={projectName}
            onChange={e => setProjectName(e.target.value)}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCancel} color='secondary'>
            Cancel
          </Button>
          <Button onClick={handleCreate} color='secondary'>
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: 'flex',
    },
    content: {
      flexGrow: 1,
      backgroundColor: 'rgba(245,245,245,1)',
      marginTop: 84,
      height: 'calc(100vh - 84px)',
      overflowY: 'auto',
      padding: 48,
    },
    addProject: {
      width: 216,
      height: 180,
      boxShadow:
        '0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)',
    },
    addIcon: {
      fontWeight: 600,
      width: '2.5em',
      height: '2.5em',
    },
  })
);

export default ProjectPage;

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { AppState } from 'index';
import { useSelector } from 'react-redux';
import LoginFirstPage from 'pages/LoginFirstPage';
import HomePage from 'pages/HomePage';
import EditorPage from 'pages/EditorPage';
import ProjectPage from 'pages/ProjectPage';
import { FirebaseState } from 'types';

const App = () => {
  const firebaseReducer = useSelector<AppState, FirebaseState>(
    state => state.firebaseReducer as FirebaseState
  );

  if (firebaseReducer.isInitializing) return <div>Loading</div>;

  return (
    <>
      {firebaseReducer.auth.isEmpty ? (
        <LoginFirstPage />
      ) : (
        <Switch>
          <Route exact path='/'>
            <HomePage />
          </Route>
          <Route exact path='/projects'>
            <ProjectPage />
          </Route>
          <Route exact path='/dashboard'>
            <EditorPage />
          </Route>
        </Switch>
      )}
    </>
  );
};

export default App;

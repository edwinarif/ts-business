import {
  NavActionTypes,
  SET_KEY_VALUE,
  ADD_LOADING_BLOCK_ID,
  DELETE_LOADING_BLOCK_ID,
  ADD_COMPONENT,
  SET_MULTI_KEY_VALUE,
  ADD_BLOCK,
  SET_COMPONENT,
  STOP_DRAGGING,
  START_DRAGGING,
  SET_DROP_CHILD_HOVER,
  SET_MERGE,
} from 'redux/actions/localActions';
import { ComponentType, IProject, IThemeFirebase, IPage } from 'types';

export type LocalReducerType = {
  propsManagerTabIdx: number;
  isAddItemOpen: boolean;
  isAddPageOpen: boolean;
  isBlockDragging: boolean;
  isPropsManagerOpen: boolean;
  currentProjectId: string;
  currentPageId: string;
  currentSelectedComponent: string;
  currentSelectedComponentPath: string;
  loadingBlockIds: string[];
  blocks: { [key: string]: ComponentType };
  components: { [key: string]: ComponentType };
  project?: IProject;
  page?: IPage;
  themes?: { [key: string]: IThemeFirebase };

  // Drag states
  isDragging: boolean;
  dragComponentId?: string;
  dragBlockId?: string;
  dragIndex?: number;
  dragPath?: string;
  dragHoverComponentId?: string;
  dragHoverIndex?: number;
  dragHoverPath?: string;
  dragDirection?: 'up' | 'down';
  dragBound?: ClientRect & DOMRect;
  dragHoverBound?: ClientRect & DOMRect;
  dragHoverIsEditor?: boolean;
};

const initialState: LocalReducerType = {
  propsManagerTabIdx: 0,
  isAddItemOpen: false,
  isAddPageOpen: false,
  isBlockDragging: false,
  isPropsManagerOpen: false,
  currentProjectId: '-Lqol3FZMvJ0hHtXeoJ1',
  currentPageId: '-LrMfQBG8ypEX6ct4C16',
  currentSelectedComponent: '',
  currentSelectedComponentPath: 'rootComponent',
  loadingBlockIds: [],
  blocks: {},
  components: {},
  isDragging: false,
};

export default (
  state = initialState,
  action: NavActionTypes
): LocalReducerType => {
  switch (action.type) {
    case SET_KEY_VALUE:
      return {
        ...state,
        [action.payload.key]: action.payload.value,
      };
    case SET_MULTI_KEY_VALUE:
    case START_DRAGGING:
      return {
        ...state,
        ...action.payload.reduce(
          (prev, current) => ({
            ...prev,
            [current.key]: current.value,
          }),
          {} as any
        ),
      };
    case SET_MERGE:
      return {
        ...state,
        [action.payload.key]: {
          ...(state[action.payload.key] as any),
          ...action.payload.value,
        },
      };

    case STOP_DRAGGING:
      return {
        ...state,
        isDragging: false,
        isBlockDragging: false,
        dragComponentId: undefined,
        dragIndex: undefined,
        dragPath: undefined,
        dragHoverComponentId: undefined,
        dragHoverIndex: undefined,
        dragHoverPath: undefined,
        dragDirection: undefined,
        dragBound: undefined,
        dragHoverBound: undefined,
        dragHoverIsEditor: undefined,
      };
    case SET_DROP_CHILD_HOVER:
      return {
        ...state,
        dragHoverIndex: action.dragHoverIndex,
        dragHoverBound: action.dragHoverBound,
        dragHoverPath: action.dragHoverPath,
        dragDirection: undefined,
        dragHoverIsEditor: undefined,
      };
    case ADD_LOADING_BLOCK_ID:
      return {
        ...state,
        loadingBlockIds: [...state.loadingBlockIds, action.blockId],
      };
    case DELETE_LOADING_BLOCK_ID:
      return {
        ...state,
        loadingBlockIds: state.loadingBlockIds.filter(
          id => id !== action.blockId
        ),
      };
    case ADD_BLOCK:
      return {
        ...state,
        blocks: {
          ...state.blocks,
          [action.block.id]: action.block,
        },
      };
    case ADD_COMPONENT:
      return {
        ...state,
        components: {
          ...state.components,
          [action.component.id]: action.component,
        },
      };
    case SET_COMPONENT:
      return {
        ...state,
        components: {
          ...state.components,
          [action.componentId]: {
            ...state.components[action.componentId],
            [action.key]: action.value,
          },
        },
      };
    default:
      return state;
  }
};

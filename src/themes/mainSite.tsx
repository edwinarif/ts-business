import red from '@material-ui/core/colors/red';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';

export const config: ThemeOptions = {
  palette: {
    primary: {
      main: 'rgba(0,121,107,1) !important',
    },
    secondary: {
      main: 'rgba(2,119,189,1) !important',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
  overrides: {
    MuiDivider: {
      root: {
        marginTop: 20,
        marginBottom: 20,
      },
    },
    MuiButton: {
      root: {
        fontFamily: "'Noto Sans', sans-serif",
        lineHeight: 'unset',
        textTransform: 'unset',
      },
    },
    MuiLink: {
      root: {
        fontFamily: "'Noto Sans', sans-serif",
      },
    },
  },
};

const theme = createMuiTheme(config);

export default theme;

import red from '@material-ui/core/colors/red';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';

export const config: ThemeOptions = {
  palette: {
    primary: {
      main: '#673AB7',
    },
    secondary: {
      main: '#FF5722',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
};

const theme = createMuiTheme(config);

export default theme;
